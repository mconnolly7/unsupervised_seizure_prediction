Logging into results/mnist_100_full0/log.txt
== COMMAND LINE ==
run.py train --encoder-layers 1000-500-250-250-250-10 --decoder-spec gauss --denoising-cost-x 1000,10,0.1,0.1,0.1,0.1,0.1 --labeled-samples 100 --unlabeled-samples 60000 --seed 1 -- mnist_100_full
== PARAMETERS ==
 zestbn              : bugfix               
 dseed               : 1                    
 top_c               : 1                    
 super_noise_std     : 0.3                  
 batch_size          : 100                  
 dataset             : mnist                
 valid_set_size      : 10000                
 num_epochs          : 150                  
 whiten_zca          : 0                    
 unlabeled_samples   : 60000                
 decoder_spec        : ('gauss',)           
 valid_batch_size    : 100                  
 denoising_cost_x    : (1000.0, 10.0, 0.1, 0.1, 0.1, 0.1, 0.1) 
 f_local_noise_std   : 0.3                  
 cmd                 : train                
 act                 : relu                 
 lrate_decay         : 0.67                 
 seed                : 1                    
 lr                  : 0.002                
 save_to             : mnist_100_full       
 save_dir            : results/mnist_100_full0 
 commit              :                      
 contrast_norm       : 0                    
 encoder_layers      : ('1000', '500', '250', '250', '250', '10') 
 labeled_samples     : 100                  
Encoder: clean, labeled
  0: noise 0
  f1: fc, relu, BN, noise 0.00, params 1000, dim (1, 784) -> (1000,)
  f2: fc, relu, BN, noise 0.00, params 500, dim (1000,) -> (500,)
  f3: fc, relu, BN, noise 0.00, params 250, dim (500,) -> (250,)
  f4: fc, relu, BN, noise 0.00, params 250, dim (250,) -> (250,)
  f5: fc, relu, BN, noise 0.00, params 250, dim (250,) -> (250,)
  f6: fc, softmax, BN, noise 0.00, params 10, dim (250,) -> (10,)
Encoder: corr, labeled
  0: noise 0.3
  f1: fc, relu, BN, noise 0.30, params 1000, dim (1, 784) -> (1000,)
  f2: fc, relu, BN, noise 0.30, params 500, dim (1000,) -> (500,)
  f3: fc, relu, BN, noise 0.30, params 250, dim (500,) -> (250,)
  f4: fc, relu, BN, noise 0.30, params 250, dim (250,) -> (250,)
  f5: fc, relu, BN, noise 0.30, params 250, dim (250,) -> (250,)
  f6: fc, softmax, BN, noise 0.30, params 10, dim (250,) -> (10,)
Decoder: z_corr -> z_est
  g6:      gauss, denois 0.10, dim None -> (10,)
  g5:      gauss, denois 0.10, dim (10,) -> (250,)
  g4:      gauss, denois 0.10, dim (250,) -> (250,)
  g3:      gauss, denois 0.10, dim (250,) -> (250,)
  g2:      gauss, denois 0.10, dim (250,) -> (500,)
  g1:      gauss, denois 10.00, dim (500,) -> (1000,)
  g0:      gauss, denois 1000.00, dim (1000,) -> (1, 784)
Found the following parameters: [f_5_b, f_4_b, f_3_b, f_2_b, f_1_b, g_6_a5, f_6_c, f_6_b, g_6_a4, g_6_a3, g_6_a2, g_6_a1, g_6_a10, g_6_a9, g_6_a8, g_6_a7, g_6_a6, g_5_a5, g_5_a4, g_5_a3, g_5_a2, g_5_a1, g_5_a10, g_5_a9, g_5_a8, g_5_a7, g_5_a6, g_4_a5, g_4_a4, g_4_a3, g_4_a2, g_4_a1, g_4_a10, g_4_a9, g_4_a8, g_4_a7, g_4_a6, g_3_a5, g_3_a4, g_3_a3, g_3_a2, g_3_a1, g_3_a10, g_3_a9, g_3_a8, g_3_a7, g_3_a6, g_2_a5, g_2_a4, g_2_a3, g_2_a2, g_2_a1, g_2_a10, g_2_a9, g_2_a8, g_2_a7, g_2_a6, g_1_a5, g_1_a4, g_1_a3, g_1_a2, g_1_a1, g_1_a10, g_1_a9, g_1_a8, g_1_a7, g_1_a6, g_0_a5, g_0_a4, g_0_a3, g_0_a2, g_0_a1, g_0_a10, g_0_a9, g_0_a8, g_0_a7, g_0_a6, f_1_W, f_2_W, f_3_W, f_4_W, f_5_W, f_6_W, g_5_W, g_4_W, g_3_W, g_2_W, g_1_W, g_0_W]
Batch norm parameters: f_1_bn_mean_clean, f_1_bn_var_clean, f_2_bn_mean_clean, f_2_bn_var_clean, f_3_bn_mean_clean, f_3_bn_var_clean, f_4_bn_mean_clean, f_4_bn_var_clean, f_5_bn_mean_clean, f_5_bn_var_clean, f_6_bn_mean_clean, f_6_bn_var_clean
Batch norm parameters: f_1_bn_mean_clean, f_1_bn_var_clean, f_2_bn_mean_clean, f_2_bn_var_clean, f_3_bn_mean_clean, f_3_bn_var_clean, f_4_bn_mean_clean, f_4_bn_var_clean, f_5_bn_mean_clean, f_5_bn_var_clean, f_6_bn_mean_clean, f_6_bn_var_clean
e 0, i 0:V_C_class inf, V_E 99.8, V_C_de 0.11 1.69 1.45 1.5 1.49 1.49 1.7
e 1, i 600:V_C_class 3.66, V_E 91.7, V_C_de 0.0112 0.148 0.981 0.921 0.55 0.24 0.108, T_C_de 0.0191 0.252 1 0.957 0.808 0.613 0.235, T_C_class 0.165
Iter 1, lr 0.002000
e 2, i 1200:V_C_class 4.93, V_E 91.2, V_C_de 0.00961 0.126 0.974 0.879 0.242 0.138 0.0811, T_C_de 0.0103 0.134 0.976 0.899 0.347 0.177 0.0931, T_C_class 0.0336
Iter 2, lr 0.002000
e 3, i 1800:V_C_class 5.81, V_E 91.1, V_C_de 0.00871 0.119 0.968 0.844 0.204 0.108 0.0678, T_C_de 0.00911 0.123 0.969 0.862 0.221 0.123 0.0755, T_C_class 0.0157
Iter 3, lr 0.002000
e 4, i 2400:V_C_class 6.17, V_E 91, V_C_de 0.00803 0.116 0.964 0.811 0.197 0.0987 0.0622, T_C_de 0.00833 0.118 0.965 0.828 0.2 0.104 0.0661, T_C_class 0.00921
Iter 4, lr 0.002000
e 5, i 3000:V_C_class 6.94, V_E 91, V_C_de 0.00766 0.108 0.962 0.783 0.187 0.0886 0.0567, T_C_de 0.00779 0.112 0.963 0.798 0.195 0.0955 0.0615, T_C_class 0.00629
Iter 5, lr 0.002000
e 6, i 3600:V_C_class 7.4, V_E 90.9, V_C_de 0.00735 0.106 0.961 0.753 0.191 0.0857 0.0512, T_C_de 0.00743 0.107 0.962 0.769 0.192 0.0906 0.0584, T_C_class 0.00457
Iter 6, lr 0.002000
e 7, i 4200:V_C_class 7.95, V_E 91, V_C_de 0.00715 0.101 0.96 0.729 0.19 0.085 0.0607, T_C_de 0.00719 0.104 0.961 0.741 0.189 0.0859 0.0562, T_C_class 0.00363
Iter 7, lr 0.002000
e 8, i 4800:V_C_class 7.99, V_E 90.9, V_C_de 0.00699 0.0985 0.96 0.697 0.184 0.0787 0.0478, T_C_de 0.00698 0.0997 0.96 0.712 0.187 0.083 0.0551, T_C_class 0.00291
Iter 8, lr 0.002000
e 9, i 5400:V_C_class 8.35, V_E 90.8, V_C_de 0.00685 0.0956 0.959 0.666 0.183 0.0802 0.0513, T_C_de 0.00684 0.0973 0.959 0.682 0.184 0.0791 0.0519, T_C_class 0.00222
Iter 9, lr 0.002000
e 10, i 6000:V_C_class 8.62, V_E 90.9, V_C_de 0.00676 0.092 0.958 0.638 0.179 0.0751 0.0479, T_C_de 0.00672 0.0939 0.958 0.655 0.182 0.078 0.0515, T_C_class 0.00191
Iter 10, lr 0.002000
e 11, i 6600:V_C_class 9.07, V_E 90.9, V_C_de 0.00668 0.0933 0.957 0.614 0.175 0.0708 0.0465, T_C_de 0.00663 0.0917 0.957 0.627 0.179 0.0755 0.0508, T_C_class 0.00159
Iter 11, lr 0.002000
Saving to results/mnist_100_full0/trained_params
e 11, i 6600:V_C_class 9.07, V_E 90.9, V_C_de 0.00668 0.0933 0.957 0.614 0.175 0.0708 0.0465, T_C_de 0.00663 0.0917 0.957 0.627 0.179 0.0755 0.0508, T_C_class 0.00159
