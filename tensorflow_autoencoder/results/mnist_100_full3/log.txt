Logging into results/mnist_100_full3/log.txt
== COMMAND LINE ==
run.py train --encoder-layers 1000-500-250-250-250-10 --decoder-spec gauss --denoising-cost-x 1000,10,0.1,0.1,0.1,0.1,0.1 --labeled-samples 100 --unlabeled-samples 60000 --seed 1 -- mnist_100_full
== PARAMETERS ==
 zestbn              : bugfix               
 dseed               : 1                    
 top_c               : 1                    
 super_noise_std     : 0.3                  
 batch_size          : 100                  
 dataset             : mnist                
 valid_set_size      : 10000                
 num_epochs          : 150                  
 whiten_zca          : 0                    
 unlabeled_samples   : 60000                
 decoder_spec        : ('gauss',)           
 valid_batch_size    : 100                  
 denoising_cost_x    : (1000.0, 10.0, 0.1, 0.1, 0.1, 0.1, 0.1) 
 f_local_noise_std   : 0.3                  
 cmd                 : train                
 act                 : relu                 
 lrate_decay         : 0.67                 
 seed                : 1                    
 lr                  : 0.002                
 save_to             : mnist_100_full       
 save_dir            : results/mnist_100_full3 
 commit              :                      
 contrast_norm       : 0                    
 encoder_layers      : ('1000', '500', '250', '250', '250', '10') 
 labeled_samples     : 100                  
Using 0 examples for validation
Encoder: clean, labeled
  0: noise 0
  f1: fc, relu, BN, noise 0.00, params 1000, dim (1, 28, 28) -> (1000,)
  f2: fc, relu, BN, noise 0.00, params 500, dim (1000,) -> (500,)
  f3: fc, relu, BN, noise 0.00, params 250, dim (500,) -> (250,)
  f4: fc, relu, BN, noise 0.00, params 250, dim (250,) -> (250,)
  f5: fc, relu, BN, noise 0.00, params 250, dim (250,) -> (250,)
  f6: fc, softmax, BN, noise 0.00, params 10, dim (250,) -> (10,)
Encoder: corr, labeled
  0: noise 0.3
  f1: fc, relu, BN, noise 0.30, params 1000, dim (1, 28, 28) -> (1000,)
  f2: fc, relu, BN, noise 0.30, params 500, dim (1000,) -> (500,)
  f3: fc, relu, BN, noise 0.30, params 250, dim (500,) -> (250,)
  f4: fc, relu, BN, noise 0.30, params 250, dim (250,) -> (250,)
  f5: fc, relu, BN, noise 0.30, params 250, dim (250,) -> (250,)
  f6: fc, softmax, BN, noise 0.30, params 10, dim (250,) -> (10,)
Decoder: z_corr -> z_est
  g6:      gauss, denois 0.10, dim None -> (10,)
  g5:      gauss, denois 0.10, dim (10,) -> (250,)
  g4:      gauss, denois 0.10, dim (250,) -> (250,)
  g3:      gauss, denois 0.10, dim (250,) -> (250,)
  g2:      gauss, denois 0.10, dim (250,) -> (500,)
  g1:      gauss, denois 10.00, dim (500,) -> (1000,)
  g0:      gauss, denois 1000.00, dim (1000,) -> (1, 28, 28)
Found the following parameters: [f_5_b, f_4_b, f_3_b, f_2_b, f_1_b, g_6_a5, f_6_c, f_6_b, g_6_a4, g_6_a3, g_6_a2, g_6_a1, g_6_a10, g_6_a9, g_6_a8, g_6_a7, g_6_a6, g_5_a5, g_5_a4, g_5_a3, g_5_a2, g_5_a1, g_5_a10, g_5_a9, g_5_a8, g_5_a7, g_5_a6, g_4_a5, g_4_a4, g_4_a3, g_4_a2, g_4_a1, g_4_a10, g_4_a9, g_4_a8, g_4_a7, g_4_a6, g_3_a5, g_3_a4, g_3_a3, g_3_a2, g_3_a1, g_3_a10, g_3_a9, g_3_a8, g_3_a7, g_3_a6, g_2_a5, g_2_a4, g_2_a3, g_2_a2, g_2_a1, g_2_a10, g_2_a9, g_2_a8, g_2_a7, g_2_a6, g_1_a5, g_1_a4, g_1_a3, g_1_a2, g_1_a1, g_1_a10, g_1_a9, g_1_a8, g_1_a7, g_1_a6, g_0_a5, g_0_a4, g_0_a3, g_0_a2, g_0_a1, g_0_a10, g_0_a9, g_0_a8, g_0_a7, g_0_a6, f_1_W, f_2_W, f_3_W, f_4_W, f_5_W, f_6_W, g_5_W, g_4_W, g_3_W, g_2_W, g_1_W, g_0_W]
Batch norm parameters: f_1_bn_mean_clean, f_1_bn_var_clean, f_2_bn_mean_clean, f_2_bn_var_clean, f_3_bn_mean_clean, f_3_bn_var_clean, f_4_bn_mean_clean, f_4_bn_var_clean, f_5_bn_mean_clean, f_5_bn_var_clean, f_6_bn_mean_clean, f_6_bn_var_clean
Batch norm parameters: f_1_bn_mean_clean, f_1_bn_var_clean, f_2_bn_mean_clean, f_2_bn_var_clean, f_3_bn_mean_clean, f_3_bn_var_clean, f_4_bn_mean_clean, f_4_bn_var_clean, f_5_bn_mean_clean, f_5_bn_var_clean, f_6_bn_mean_clean, f_6_bn_var_clean
e 0, i 0:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan
e 1, i 600:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 833 1.59 1.79 1.78 1.4 1.25 1.11, T_C_class 1.96
Iter 1, lr 0.002000
e 2, i 1200:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 14.1 1.63 2.1 2.61 1.64 1.18 1.11, T_C_class 2.03
Iter 2, lr 0.002000
e 3, i 1800:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 4.57 1.74 2.08 2.68 1.83 1.23 1.07, T_C_class 2.05
Iter 3, lr 0.002000
e 4, i 2400:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 2.36 1.84 1.99 2.58 2.05 1.3 1.12, T_C_class 2.09
Iter 4, lr 0.002000
e 5, i 3000:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 2.02 2.05 1.72 2.15 1.84 1.35 1.18, T_C_class 2.26
Iter 5, lr 0.002000
e 6, i 3600:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 1.71 2.2 1.59 1.87 1.76 1.39 1.19, T_C_class 2.35
Iter 6, lr 0.002000
e 7, i 4200:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 1.25 2.27 1.52 1.71 1.69 1.43 1.29, T_C_class 2.36
Iter 7, lr 0.002000
e 8, i 4800:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 1.65 2.76 1.4 1.41 1.46 1.33 1.29, T_C_class 2.51
Iter 8, lr 0.002000
e 9, i 5400:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 1.28 2.83 1.32 1.3 1.36 1.29 1.26, T_C_class 2.6
Iter 9, lr 0.002000
e 10, i 6000:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.798 2.72 1.31 1.28 1.33 1.3 1.32, T_C_class 2.69
Iter 10, lr 0.002000
e 11, i 6600:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.574 2.7 1.31 1.26 1.4 1.38 1.42, T_C_class 2.74
Iter 11, lr 0.002000
e 12, i 7200:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.389 2.72 1.31 1.24 1.41 1.43 1.48, T_C_class 2.72
Iter 12, lr 0.002000
e 13, i 7800:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.338 2.68 1.33 1.25 1.43 1.48 1.57, T_C_class 2.72
Iter 13, lr 0.002000
e 14, i 8400:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.29 2.76 1.31 1.24 1.42 1.49 1.6, T_C_class 2.68
Iter 14, lr 0.002000
e 15, i 9000:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.189 2.64 1.32 1.25 1.44 1.6 1.94, T_C_class 2.75
Iter 15, lr 0.002000
e 16, i 9600:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.315 2.52 1.35 1.24 1.34 1.48 1.45, T_C_class 2.66
Iter 16, lr 0.002000
e 17, i 10200:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.145 2.37 1.35 1.24 1.36 1.48 1.36, T_C_class 2.66
Iter 17, lr 0.002000
e 18, i 10800:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.183 2.32 1.35 1.22 1.32 1.46 1.44, T_C_class 2.59
Iter 18, lr 0.002000
e 19, i 11400:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.105 2.23 1.35 1.22 1.31 1.47 1.46, T_C_class 2.52
Iter 19, lr 0.002000
e 20, i 12000:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.138 2.16 1.34 1.22 1.28 1.44 1.4, T_C_class 2.43
Iter 20, lr 0.002000
e 21, i 12600:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0881 2.05 1.34 1.23 1.27 1.44 1.39, T_C_class 2.35
Iter 21, lr 0.002000
e 22, i 13200:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0914 1.88 1.35 1.24 1.26 1.42 1.41, T_C_class 2.29
Iter 22, lr 0.002000
e 23, i 13800:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.104 1.76 1.32 1.23 1.28 1.44 1.36, T_C_class 2.25
Iter 23, lr 0.002000
e 24, i 14400:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.09 1.63 1.31 1.24 1.28 1.42 1.36, T_C_class 2.2
Iter 24, lr 0.002000
e 25, i 15000:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.141 2.68 1.27 1.2 1.24 1.4 1.45, T_C_class 2.43
Iter 25, lr 0.002000
e 26, i 15600:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0811 2.6 1.27 1.19 1.24 1.39 1.38, T_C_class 2.43
Iter 26, lr 0.002000
e 27, i 16200:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0807 2.41 1.27 1.19 1.24 1.36 1.37, T_C_class 2.39
Iter 27, lr 0.002000
e 28, i 16800:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0777 2.22 1.27 1.18 1.25 1.34 1.34, T_C_class 2.33
Iter 28, lr 0.002000
e 29, i 17400:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0767 2.02 1.28 1.18 1.25 1.32 1.32, T_C_class 2.28
Iter 29, lr 0.002000
e 30, i 18000:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0764 1.83 1.29 1.18 1.25 1.3 1.29, T_C_class 2.25
Iter 30, lr 0.002000
e 31, i 18600:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0775 1.66 1.3 1.18 1.24 1.29 1.27, T_C_class 2.2
Iter 31, lr 0.002000
e 32, i 19200:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0752 1.48 1.32 1.17 1.25 1.27 1.24, T_C_class 2.13
Iter 32, lr 0.002000
e 33, i 19800:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0745 1.33 1.34 1.18 1.24 1.25 1.21, T_C_class 2.04
Iter 33, lr 0.002000
e 34, i 20400:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0786 1.25 1.32 1.17 1.23 1.24 1.19, T_C_class 1.95
Iter 34, lr 0.002000
e 35, i 21000:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0753 1.19 1.32 1.17 1.23 1.24 1.17, T_C_class 1.86
Iter 35, lr 0.002000
e 36, i 21600:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0737 1.12 1.32 1.17 1.23 1.22 1.16, T_C_class 1.71
Iter 36, lr 0.002000
e 37, i 22200:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.0728 1.07 1.32 1.17 1.22 1.2 1.15, T_C_class 1.58
Iter 37, lr 0.002000
e 38, i 22800:V_C_class nan, V_E nan, V_C_de nan nan nan nan nan nan nan, T_C_de 0.072 1.04 1.3 1.18 1.22 1.19 1.17, T_C_class 1.33
Iter 38, lr 0.002000
