import tensorflow.contrib.layers as lays
import scipy.io as sio
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


def encoder(net, phase):
    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=2000, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=1000, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=500, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=250, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=100, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net)
    net     = lays.fully_connected(inputs=net, num_outputs=80, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net)
    net     = lays.fully_connected(inputs=net, num_outputs=60, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net)
    net     = lays.fully_connected(inputs=net, num_outputs=30, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net)
    net     = lays.fully_connected(inputs=net, num_outputs=10, activation_fn=tf.nn.tanh)
    return net


def decoder(net,phase):
    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=10, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=30, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=60, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=80, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=100, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=250, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=500, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=1000, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=2000, activation_fn=tf.nn.tanh)

    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=1000, activation_fn=tf.nn.tanh)

    return net


def load_data():
    data_struct = sio.loadmat('../data/sz_data-256Hz-5s_window_norm.mat')
    data = data_struct['data_mat'].T
    labels = data_struct['sz_label_mat']
    data = (data - np.mean(data, axis=1, keepdims=True)) / np.std(data,axis=1, keepdims=True) / 2
    return data, labels


