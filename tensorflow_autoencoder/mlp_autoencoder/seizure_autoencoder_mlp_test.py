from mlp_autoencoder.seizure_autoencoder_mlp import *
import numpy as np
import tensorflow as tf

ae_inputs           = tf.placeholder(tf.float32, (None, 1000))  # input to the network (MNIST images)
phase               = tf.placeholder(tf.bool, name='phase')

ae_encoded          = encoder(ae_inputs,phase)
ae_outputs          = decoder(encoder(ae_inputs,phase),phase)
loss                = tf.reduce_mean(tf.square(ae_outputs - ae_inputs))  # calculate the mean square error loss

init                = tf.global_variables_initializer()

X_train, Y_train     = load_data()
print(X_train.shape)
with tf.Session() as sess:
    sess.run(init)

    saver = tf.train.import_meta_graph('mlp_autoencoder.meta')
    saver.restore(sess, tf.train.latest_checkpoint('./'))

    # test the trained network
    batch_idx = np.random.choice(X_train.shape[0], 50)
    print(batch_idx[0:9])

    # batch_img = X_train[batch_idx]

    batch_img = X_train
    recon_img = sess.run([ae_encoded], feed_dict={ae_inputs: batch_img,'phase:0': 0})[0]

    c       = sess.run([loss], feed_dict={ae_inputs: batch_img,'phase:0': 0})

    print(c)
    for c1 in range(10):
        plt.subplot(10,1,c1+1)
        plt.plot(recon_img[batch_idx[c1],:])
        print(batch_idx[c1])
        plt.plot(X_train[batch_idx[c1],:])

    plt.show()

    np.savez('mlp_temp.npz', X=recon_img, Y=Y_train)