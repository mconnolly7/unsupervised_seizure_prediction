from __future__ import division, print_function, absolute_import
from mlp_autoencoder.seizure_autoencoder_mlp import *
import tensorflow as tf
import tensorflow.contrib.layers as lays
import numpy as np

batch_size  = 100       # Number of samples in each batch
epoch_num   = 500         # Number of epochs to train the network
lr          = 0.001     # Learning rate

# read seizure data from mat file
X_train, Y_train  = load_data()

# calculate the number of batches per epoch
batch_per_ep = X_train.shape[0] // batch_size

# build the network
ae_inputs   = tf.placeholder(tf.float32, (None, 1000))  # input to the network
phase       = tf.placeholder(tf.bool, name='phase')
ae_outputs  = decoder(encoder(ae_inputs,phase),phase)  # create the Autoencoder network

# Calculate the loss and optimize the network
loss        = tf.reduce_mean(tf.square(ae_outputs - ae_inputs))  # calculate the mean square error loss

# initialize the network
update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
with tf.control_dependencies(update_ops):
    # Ensures that we execute the update_ops before performing the train_step
    train_op = tf.train.AdamOptimizer(learning_rate=lr).minimize(loss)

init        = tf.global_variables_initializer()
saver       = tf.train.Saver()
with tf.Session() as sess:
    sess.run(init)

    for ep in range(epoch_num):  # epochs loop
        for batch_n in range(batch_per_ep):  # batches loop
            batch_idx   = np.random.choice(X_train.shape[0], batch_size)
            batch_img   = X_train[batch_idx]

            _, c = sess.run([train_op, loss], feed_dict={ae_inputs: batch_img, 'phase:0': 1})
        print('Epoch: {} - cost= {:.5f}'.format((ep + 1), c))

        if np.mod(ep,10) == 0:
            saver.save(sess, 'mlp_autoencoder')