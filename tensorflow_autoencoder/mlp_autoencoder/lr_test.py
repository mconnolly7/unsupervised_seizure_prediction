from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt
import numpy as np

data = np.load('encoded_data.npz')
# _, labels = load_data()
data_struct = sio.loadmat('./data/sz_data-256Hz-5s_window_norm.mat')
Y           = data_struct['sz_label_mat'].T

X = data['data_enc']
Y = np.squeeze(data['Y'])

print(X.shape)
print(Y.shape)

x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size=0.25)
print(x_train.shape)

# all parameters not specified are set to their defaults
plt.figure(1)
logisticRegr    = LogisticRegression()
logisticRegr.fit(x_train, y_train)
predictions     = logisticRegr.predict(x_test)
# plt.plot(predictions)
# plt.plot(labels.T)

plt.figure(2)
score               = logisticRegr.score(x_test, y_test)
fpr, tpr, _         = metrics.roc_curve(y_test, predictions)
plt.plot(fpr,tpr)
print(score)
plt.show()
