from __future__ import division, print_function, absolute_import
from convolutional_autoencoder.mnist_autoencoder_2D import *
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from skimage import transform

batch_size  = 500       # Number of samples in each batch
epoch_num   = 1         # Number of epochs to train the network
lr          = 0.001     # Learning rate

ae_inputs   = tf.placeholder(tf.float32, (None, 32, 32, 1))  # input to the network (MNIST images)
ae_outputs  = decoder(encoder(ae_inputs))  # create the Autoencoder network

# Calculate the loss and optimize the network
loss        = tf.reduce_mean(tf.square(ae_outputs - ae_inputs))  # calculate the mean square error loss
train_op    = tf.train.AdamOptimizer(learning_rate=lr).minimize(loss)

# initialize the network
init        = tf.global_variables_initializer()

# read MNIST dataset from CSV
X_train, Y_train, X_test, Y_test = load_data()

# calculate the number of batches per epoch
batch_per_ep = Y_train.shape[0] // batch_size

with tf.Session() as sess:
    sess.run(init)

    for ep in range(epoch_num):  # epochs loop
        for batch_n in range(batch_per_ep):  # batches loop
            batch_idx   = np.random.choice(X_train.shape[0], batch_size)
            batch_img   = X_train[batch_idx]
            batch_img   = batch_img.reshape((-1, 28, 28, 1))               # reshape each sample to an (28, 28) image

            batch_img   = resize_batch(batch_img)                          # reshape the images to (32, 32)
            _, c = sess.run([train_op, loss], feed_dict={ae_inputs: batch_img})
            print('Epoch: {} - cost= {:.5f}'.format((ep + 1), c))

    saver = tf.train.Saver()
    saver.save(sess, 'my-test-model')

