import tensorflow.contrib.layers as lays

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from skimage import transform


def encoder(inputs):
    # encoder
    # 32 x 32 x 1   ->  16 x 16 x 32
    # 16 x 16 x 32  ->  8 x 8 x 16
    # 8 x 8 x 16    ->  2 x 2 x 8
    net     = lays.conv2d(inputs, 32, [5, 5], stride=2, padding='SAME')
    net     = lays.conv2d(net, 16, [5, 5], stride=2, padding='SAME')
    net     = lays.conv2d(net, 8, [5, 5], stride=4, padding='SAME')

    net     = tf.reshape(net, (-1, 32))
    net     = lays.fully_connected(inputs=net, num_outputs=32, activation_fn=tf.nn.relu)

    return net


def decoder(net):

    net = lays.fully_connected(inputs=net, num_outputs=32, activation_fn=tf.nn.relu)
    net = tf.reshape(net, (-1, 2, 2, 8))

    # decoder
    # 2 x 2 x 8    ->  8 x 8 x 16
    # 8 x 8 x 16   ->  16 x 16 x 32
    # 16 x 16 x 32  ->  32 x 32 x 1
    net = lays.conv2d_transpose(net, 16, [5, 5], stride=4, padding='SAME')
    net = lays.conv2d_transpose(net, 32, [5, 5], stride=2, padding='SAME')
    decoded = lays.conv2d_transpose(net, 1, [5, 5], stride=2, padding='SAME', activation_fn=tf.nn.tanh)
    return decoded


def resize_batch(imgs):
    # A function to resize a batch of MNIST images to (32, 32)
    # Args:
    #   imgs: a numpy array of size [batch_size, 28 X 28].
    # Returns:
    #   a numpy array of size [batch_size, 32, 32].
    imgs            = imgs.reshape((-1, 28, 28, 1))
    resized_imgs    = np.zeros((imgs.shape[0], 32, 32, 1))

    for i in range(imgs.shape[0]):
        resized_imgs[i, ..., 0] = transform.resize(imgs[i, ..., 0], (32, 32), mode='constant')

    return resized_imgs

def csv_to_numpy_array(filename, delimiter=','):
    data = np.genfromtxt(filename, delimiter=delimiter)
    return data


def load_data():
    test_data           = csv_to_numpy_array('../data/mnist_test.csv')
    train_data          = csv_to_numpy_array('../data/mnist_train.csv')
    # [test_data, train_data] = np.vsplit(test_data, [1000])

    [Y_test, X_test]    = np.hsplit(test_data,[1])
    [Y_train, X_train]  = np.hsplit(train_data, [1])

    X_train             = X_train / 255
    return X_train, Y_train, X_test, Y_test