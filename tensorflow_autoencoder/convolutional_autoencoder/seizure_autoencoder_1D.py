import tensorflow.contrib.layers as lays
import scipy.io as sio
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

# import tensorflow.python.layers.convolutional as lays

def encoder(inputs):
    # encoder
    # 256 x 1   ->  128 x 32
    # 128 x 32  ->  64 x 16
    # 64 x 16    ->  16 x 8
    # 16 x 8    -> 128

    print(inputs)
    net     = lays.conv2d(inputs, 32, 5, stride=2, padding='SAME')
    print(net)
    net     = lays.conv2d(net, 16, 5, stride=2, padding='SAME')
    print(net)
    net     = lays.conv2d(net, 8, 5, stride=4, padding='SAME')
    print(net)
    net     = tf.reshape(net, (-1, 128))
    print(net)
    net     = lays.fully_connected(inputs=net, num_outputs=128, activation_fn=tf.nn.relu)
    print(net)
    return net


def decoder(net):

    net = lays.fully_connected(inputs=net, num_outputs=128, activation_fn=tf.nn.relu)
    net = tf.reshape(net, (-1, 1, 16, 8))
    print(net)

    # decoder
    # 2 x 2 x 8    ->  8 x 8 x 16
    # 8 x 8 x 16   ->  16 x 16 x 32
    # 16 x 16 x 32  ->  32 x 32 x 1
    net = lays.conv2d_transpose(net, 16, [5,1], stride=4, padding='SAME')
    print(net)
    net = lays.conv2d_transpose(net, 32, [5,1], stride=2, padding='SAME')
    print(net)
    decoded = lays.conv2d_transpose(net, 1, [5,1], stride=2, padding='SAME', activation_fn=tf.nn.tanh)
    return decoded


def load_data():
    data = sio.loadmat('../data/sz_data-256Hz-1s_window_test.mat');
    # print(data['data_mat'])
    # plt.plot(data['data_mat'][:,0])
    # plt.show()
    return  data['data_mat'].T

