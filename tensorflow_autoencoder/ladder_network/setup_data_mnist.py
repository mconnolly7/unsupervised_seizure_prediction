import numpy as np
import h5py
from fuel.datasets.hdf5 import H5PYDataset
from fuel.datasets import MNIST, CIFAR10
from fuel import config
config.data_path = '/Users/mconnolly/PycharmProjects/UG3_autoencoder/data'



def create_mnist():
    test_data           = np.loadtxt('data/mnist_test.csv', delimiter=",")
    test_features       = test_data[:,1:]
    test_labels         = test_data[:,0]
    test_labels         = test_labels[np.newaxis]
    test_labels         = test_labels.T

    train_data          = np.loadtxt('data/mnist_train.csv', delimiter=",")
    train_features      = train_data[:,1:]
    train_labels        = train_data[:,0]
    train_labels        = train_labels[np.newaxis]
    train_labels        = train_labels.T

    n_test              = np.size(test_labels)
    n_train             = np.size(train_labels)

    for c1 in range(0,9):
        idx = np.where(train_labels == c1)
        train_labels[idx[0][10:]] = -1

    labels_all          = np.vstack([test_labels, train_labels])
    features_all        = np.vstack([test_features, train_features])

    f                   = h5py.File('data/mnist_dataset.hdf5', mode='w')

    labels              = f.create_dataset('targets', labels_all.shape, dtype='int')
    features            = f.create_dataset('features', features_all.shape, dtype='float32')

    labels[...]         = labels_all
    features[...]       = features_all / 255

    split_dict = {
        'train': {'features': (0, n_train), 'targets': (0, n_train)},
        'test': {'features': (n_train, n_train+n_test),'targets': (n_train, n_train+n_test)}
    }

    f.attrs['split'] = H5PYDataset.create_split_array(split_dict)

    f.flush()
    f.close()

# create_mnist()

# data = H5PYDataset(
#     'data/mnist_dataset.hdf5', which_sets=('train',),
#     sources=['features', 'labels'], load_in_memory=True)

train_set = H5PYDataset('data/mnist_dataset.hdf5', which_sets=('train',), load_in_memory=True)


dataset_class, training_set_size = (MNIST, 50000)
builtin_set = dataset_class(["train"])

print builtin_set.provides_sources
print train_set.provides_sources
print '----'
print builtin_set.data_sources[0].shape
print train_set.data_sources[0].shape