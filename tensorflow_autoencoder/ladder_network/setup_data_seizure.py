import numpy as np
import h5py
from fuel.datasets.hdf5 import H5PYDataset

import scipy.io as sio

feature_vector  = sio.loadmat('data/sz_data-250Hz-1s_window_features_raw.mat')
labels_vector   = sio.loadmat('data/sz_data-250Hz-1s_window_labels.mat')

t_labels        = labels_vector['t_labels'].T
raw_features    = feature_vector['data_mat'].T

f               = h5py.File('sz_dataset.hdf5', mode='w')

labels          = f.create_dataset('labels', t_labels.shape, dtype='int')
features        = f.create_dataset('features', raw_features.shape, dtype='float32')

labels[...]     = t_labels
features[...]   = raw_features
split_dict = {
    'train': {'features': (0, 6353), 'labels': (0, 6353)},
    'test': {'features': (6353, 7727),'labels': (6353, 7727)}
}

f.attrs['split'] = H5PYDataset.create_split_array(split_dict)

f.flush()
f.close()

data = H5PYDataset(
    'data/sz_dataset.hdf5', which_sets=('train',),
    sources=['features', 'labels'], load_in_memory=True)
a = data.data_sources[1]
b = np.where(a == -1)
print np.size(b)

