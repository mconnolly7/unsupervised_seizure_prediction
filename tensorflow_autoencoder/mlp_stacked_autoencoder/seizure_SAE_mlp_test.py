from mlp_stacked_autoencoder.seizure_SAE_mlp import *
import numpy as np
import tensorflow as tf

# build the network
# build the network
original_data   = tf.placeholder(tf.float32, (None, 256))  # input to the network
phase           = tf.placeholder(tf.bool, name='phase')

# encoded_pre     = lays.batch_norm(original_data, is_training=phase)
encoded_pre     = original_data
encoded_data    = lays.fully_connected(inputs=encoded_pre,
                                       num_outputs=512,
                                       weights_regularizer=lays.l2_regularizer(scale=0.001),
                                       activation_fn=tf.nn.relu)
reconst_data    = lays.fully_connected(inputs=encoded_pre, num_outputs=256, activation_fn=tf.nn.tanh)

# Calculate the loss and optimize the network
loss            = tf.reduce_mean(tf.square(reconst_data - original_data))  # calculate the mean square error loss

X_train, Y_train    = load_data()
init                = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)

    saver = tf.train.import_meta_graph('mlp_stacked_autencoder.meta')
    saver.restore(sess, tf.train.latest_checkpoint('./'))

    # test the trained network
    batch_idx = np.random.choice(X_train.shape[0], 50)
    print(batch_idx[0:9])
    print(batch_idx.shape)

    batch_data = X_train[batch_idx]
    reconst_ex = sess.run([reconst_data], feed_dict={original_data: batch_data,'phase:0': 0})[0]

    cost       = sess.run([loss], feed_dict={original_data: batch_data,'phase:0': 0})
    print(cost)

    for c1 in range(10):
        plt.subplot(10,1,c1+1)
        plt.plot(reconst_ex[c1,:])
        print(batch_idx[c1])
        plt.plot(X_train[batch_idx[c1],:])

    plt.show()
    # c0 = np.squeeze(Y_train == 0)
    # c1 = np.squeeze(Y_train == 1)
    #
    # print(c0.shape)
    # plt.scatter(recon_img[c0,0], recon_img[c0,1])
    # plt.scatter(recon_img[c1,0], recon_img[c1,1])
    #
    # plt.show()

    # np.savez('mlp_temp.npz', X=recon_img, Y=Y_train)