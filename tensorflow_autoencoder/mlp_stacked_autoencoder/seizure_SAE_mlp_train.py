from __future__ import division, print_function, absolute_import
from mlp_autoencoder.seizure_autoencoder_mlp import *
import tensorflow as tf
import tensorflow.contrib.layers as lays
import numpy as np

batch_size  = 100       # Number of samples in each batch
epoch_num   = 500         # Number of epochs to train the network
lr          = 0.01     # Learning rate

# read seizure data from mat file
X_train, Y_train = load_data()

# calculate the number of batches per epoch
batch_per_ep    =  X_train.shape[0] // batch_size

layers          = [input_dim, 512, 256, 128, 64, 32, 16, 8]
# build the network
original_data   = tf.placeholder(tf.float32, (None, 256))  # input to the network
phase           = tf.placeholder(tf.bool, name='phase')

# encoded_pre     = lays.batch_norm(original_data, is_training=phase)
encoded_pre     = original_data
encoded_data    = lays.fully_connected(inputs=encoded_pre,
                                       num_outputs=512,
                                       weights_regularizer=lays.l2_regularizer(scale=0.001),
                                       activation_fn=tf.nn.relu)
reconst_data    = lays.fully_connected(inputs=encoded_pre, num_outputs=256, activation_fn=tf.nn.tanh)

# Calculate the loss and optimize the network
loss            = tf.reduce_mean(tf.square(reconst_data - original_data))  # calculate the mean square error loss

# initialize the network
update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
with tf.control_dependencies(update_ops):
    # Ensures that we execute the update_ops before performing the train_step
    train_op = tf.train.AdamOptimizer(learning_rate=lr).minimize(loss)

init    = tf.global_variables_initializer()
saver   = tf.train.Saver()

with tf.Session() as sess:
    sess.run(init)

    for ep in range(epoch_num):  # epochs loop
        for batch_n in range(batch_per_ep):  # batches loop
            batch_idx   = np.random.choice(X_train.shape[0], batch_size)
            batch_data  = X_train[batch_idx]

            _, c = sess.run([train_op, loss], feed_dict={original_data: batch_data, 'phase:0': 1})
        print('Epoch: {} - cost= {:.5f}'.format((ep + 1), c))

        if np.mod(ep,10) == 0:
            saver.save(sess, 'mlp_stacked_autencoder')