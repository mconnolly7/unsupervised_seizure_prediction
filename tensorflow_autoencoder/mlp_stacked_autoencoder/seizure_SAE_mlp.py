import tensorflow.contrib.layers as lays
import scipy.io as sio
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


def batch_norm_layer(net, num_outputs, phase):
    net     = lays.batch_norm(net, is_training=phase)
    net     = lays.fully_connected(inputs=net, num_outputs=num_outputs, activation_fn=tf.nn.relu)

    return net


def load_data():
    data_struct = sio.loadmat('../data/sz_data-256Hz-1s_window_norm.mat')
    data = data_struct['data_mat'].T
    labels = data_struct['sz_label_mat']
    data = (data - np.mean(data, axis=1, keepdims=True)) / np.std(data,axis=1, keepdims=True) / 2
    return data, labels


