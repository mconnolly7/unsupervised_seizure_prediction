%%
clear; openNSx('workspace/raw_data/UG3_data/Waldo_data/Waldo_2017_11_29/waldo11292017008.ns2')
% % 
%%
sz_table = readtable('workspace/raw_data/UG3_data/Waldo_data/Waldo_2017_11_29/Waldo_seizure_data_2018_11_29.csv');
sz_table = sz_table(sz_table.Var1 == 7,:);

%%
fold_time       = 5;
fold_samples    = fold_time * 200;


data            = double(NS2.Data(1,:));
data            = resample(data,1,5);
data            = (data - mean(data)) ./ std(data);
mod_len         = mod(size(data,2),fold_samples);

data            = data(1:end-mod_len);
data_mat        = reshape(data, fold_samples, []);
t               = fold_time:fold_time:size(data_mat,2)*fold_time;

% [a b c]             = zscore(data_mat);
% data_mat(:,c > 3)   = [];
% t(c > 3)            = [];
% sz_label            = zeros(size(t));
% [coeff,score,latent,tsquared,explained,mu] = pca(data_mat');
% 
% 
% for c1 = 1:size(data_mat,2)
%    subplot(2,1,1)
%    plot(data_mat(c1,1:100), data_mat(c1,6:105));
%    xlim([-4 4])
%    ylim([-4 4]);
    %    
%    subplot(2,1,2)
%    plot(data_mat(:,c1))
%    title(num2str(t(c1)))
%    drawnow
%    pause(.1);
% end
% 
% % idx = randperm(size(score,1));
% 
% % scatter3(score(idx(1:100),1), score(idx(1:100),2), score(idx(1:100),3))

%%
sz_t    = zeros(size(t));
d       = data_enc{end};

figure;
hold on;
for c1 = 1:size(sz_table,1)
    sz_start_t =  sz_table.Var2(c1);
    sz_end_t =  sz_table.Var3(c1);
    [~, sz_start_idx] = min(abs(t - sz_start_t ));
    
    d_start_idx = sz_start_idx - 12;
    d_end_idx   = d_start_idx + 24; 
    
    dd(c1,:) = d(1,d_start_idx:d_end_idx);
    
    patch([sz_start_t sz_end_t sz_end_t sz_start_t], [0 0 1 1], [.5 .5 .5], 'facealpha', .5)
end

plot(t,d, 'linewidth', 2);
%%
% layers = [ 0 30 10 3];
layers = [ 0 2000 1000 500 250 100 80 60 30 10 3];
data_enc{1} = data_mat;
for c1 = 2:size(layers,2)
    d_e             = data_enc{c1 - 1}';
    [d_n, a, b]     = zscore(d_e);
    
    auto_enc{c1}    = trainAutoencoder(d_n', layers(c1));
    data_enc{c1}    = encode(auto_enc{c1},d_n');
    data_recon{c1}  = auto_enc{c1}.predict(d_e');
end

%%
clc
data_enc_test{1} = data_mat;
for c1 = 2:size(auto_enc,2)
    d_e             = data_enc_test{c1 - 1}';
    [d_n, a, b]     = zscore(d_e);
    
    data_enc_test{c1}   = encode(auto_enc{c1},d_n');
    data_recon{c1}      = auto_enc{c1}.predict(d_e');
end

%%
data_enc_2  = encode(autoenc_2, data_enc);
for c1 = 1:10
    [idx, C, sumd]  = kmeans(data_enc',c1);
    d(c1) = mean(sumd);
end

%%
[idx, C, sumd]  = kmeans(data_enc_2',10);

subplot(2,1,1)
histogram(idx(sz_label == 0), 'normalization', 'pdf')
subplot(2,1,2)
histogram(idx(sz_label == 1), 'normalization', 'pdf')

%%
score = data_enc';
ii_score = score(sz_label == 0,:);
sz_score = score(sz_label == 1, :);

subplot(1,2,1)
hold on
scatter(ii_score(:,1), ii_score(:,2))
scatter(sz_score(:,1), sz_score(:,2))

subplot(1,2,2)
hold on
scatter3(ii_score(:,1), ii_score(:,2), ii_score(:,3))
scatter3(sz_score(:,1), sz_score(:,2), sz_score(:,3))
cumsum(explained(1:5))
