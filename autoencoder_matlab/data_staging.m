%%
clc
close all
data_dir = 'workspace/raw_data/UG3_data/Waldo_data/';

exp_dates = {'waldo_2017_12_20', 'waldo_2018_02_07', 'waldo_2018_02_14', ...
    'waldo_2018_03_01', 'waldo_2018_03_06'};

exp_date    = 4;
exp_num     = '007.ns2';
fs          = 1000;
file_name   = [data_dir exp_dates{exp_date} filesep exp_dates{exp_date} '_' exp_num];
disp(file_name)

ns2         = openNSx(file_name);

if iscell(ns2.Data)
    data = [];
    for c1 = 1:size(ns2.Data,2)
        data    = [data ns2.Data{c1}];
    end
else
    data    = ns2.Data;
end

data        = double(data);
plot((1:length(data))/fs,data(1,:))
% plot(data(1,:))

%%


matrixToNSx(data(:,2.5e6:5e6), 1000, 'uv',  [file_name '_011'])
matrixToNSx(data(:,1:2.5e6), 1000, 'uv',  [file_name '_012'])

%%
exp_num     = '00x.ns2';
file_name   = [data_dir exp_dates{exp_date} filesep exp_dates{exp_date} '_' exp_num];
openNSx(file_name)
% 2/1 - nothing
% 2/2 - nothing


%%









%%

data_mat    = [];
label_mat   = [];
stim_table = readtable([data_dir 'waldo12202017_stimulations_2.csv']);
offset = 2;
for c1 = 1:12 %size(stim_table,1)
    stim_off    = (stim_table.stimulation_end(c1) + offset) * sampling_frequency;
    stim_start  = (stim_table.stimulation_start(c1+1) - offset) * sampling_frequency;
    data_seg    = data(stim_off:stim_start);
    
    index       = 1;
    data_stream = [];
    
    for c2 = 1000:100:size(data_seg,2)
        data_stream(index,:) = data_seg(c2-999:c2);
        index = index+1;
    end
    
    label_stream = stim_table.amplitude(c1)/300 * ones(size(data_stream,1),1);

    data_mat    = [data_mat; data_stream];
    label_mat   = [label_mat; label_stream];
    
end


%%
for c1 = 1:3
    subplot(3,1,c1)
    plot(de(:,c1))
end

%%

  clc  

% de = double(de_est);

x1 = de(1:end-1,:);
x2 = de(2:end,:);
delta_x = x2-x1;
t       = (1:size(de,1))/2;

idx = encoded_label(1:end-1);
c1 = 1;
subplot(3,4,1:3)
scatter(t(idx==0),de(idx==0,c1),'.')
hold on
scatter(t(idx==1),de(idx==1,c1),'.')
title('Encoded Dimension 1')
ylabel('X1 (a.u.)')
set(gca, 'FontSize', 16)

subplot(3,4,4)
scatter(x1(idx==0,c1), delta_x(idx==0,c1),'.')
hold on
scatter(x1(idx==1,c1), delta_x(idx==1,c1),'.')

xlabel('dX1')
ylabel('X1')
set(gca, 'FontSize', 16)

c1 = 2;
subplot(3,4,5:7)
scatter(t(idx==0),de(idx==0,c1),'.')
hold on
scatter(t(idx==1),de(idx==1,c1),'.')
title('Encoded Dimension 2')
ylabel('X2 (a.u.)')
set(gca, 'FontSize', 16)

subplot(3,4,8)
scatter(x1(idx==0,c1), delta_x(idx==0,c1),'.')
hold on
scatter(x1(idx==1,c1), delta_x(idx==1,c1),'.')
xlabel('dX2')
ylabel('X2')
set(gca, 'FontSize', 16)

c1 = 3;
subplot(3,4,9:11)
scatter(t(idx==0),de(idx==0,c1),'.')
hold on
scatter(t(idx==1),de(idx==1,c1),'.')
title('Encoded Dimension 3')
xlabel('Time (approx.)')
ylabel('x3 (a.u.)')
set(gca, 'FontSize', 16)

legend({'Stimulation', 'Baseline'})
subplot(3,4,12)
scatter(x1(idx==0,c1), delta_x(idx==0,c1),'.')
hold on
scatter(x1(idx==1,c1), delta_x(idx==1,c1),'.')
xlabel('dX3')
ylabel('X3')
set(gca, 'FontSize', 16)

%%
for c1 = 3
feature = c1;
    de = double(de);
    gp_model = gp_object();
    gp_model.initialize_data((1:1000)',de(1:1000,feature))
    gp_model.minimize(10)

    h = gp_model.hyperparameters;
    t = (1:size(de,1))';

    gp_model.initialize_data((1:size(de,1))', de(:,feature));
    gp_model.hyperparameters = h;
    de_est(:,feature) = gp_model.predict(t);
end
%%
y1      = y_est(1:end-1);
y2      = y_est(2:end);
e       = encoded_label(1:end-1);
delta_y = y2 - y1;
scatter(y1(e==0), delta_y(e==0));
hold on
scatter(y1(e==1), delta_y(e==1));

%%
de      = de_est;
de      = double(encoded_data{end});
f       = 1;
x1      = de(1:end-1,:);
x2      = de(2:end,:);
delta_x = x2-x1;

idx_1   = mod(1:size(x1,1),1) == 0;
idx_2   = encoded_label(1:end-1) == 1;
idx     = idx_1 & idx_2';

d_model = gp_object();
d_model.initialize_data(x1(idx,f),delta_x(idx,f));
d_model.minimize(4)
t = linspace(min(x1(:,f)),max(x1(:,f)),300)';


[y,~,~, ys] = d_model.predict(t);
plot(t,[y y+ys y-ys], 'r')
hold on

%%

d      = de_est;
% d      = double(encoded_data{end});
f       = 1;
x1      = de(1:end-1,:);
x2      = de(2:end,:);

for c1 = 1:3
    for c2 = 1:3
        subplot(3,3, (c1-1)*3+c2)
        hold on
        
        scatter(x1(idx==0,c1), x2(idx==0,c2)); 
        scatter(x1(idx==1,c1), x2(idx==1,c2));
        
        xlabel(sprintf('x%d_t', c1))
        ylabel(sprintf('x%d_{t+1}', c2))
    end
    
end


%%
data_dir = '/Users/mconnolly/Intelligent Control Repository/workspace/raw_data/UG3_data/waldo_window_data/';

data_files  = dir([data_dir '*.mat']);

data_mat    = [];
policy_mat  = [];
label_mat   = [];

for c1 = 1:size(data_files)
    file_path = [data_dir data_files(c1).name];
    data = load(file_path);
    
    data_mat = [data_mat; data.segmented_data];
    policy_mat = [policy_mat; data.policy_data];
    label_mat = [label_mat; data.label_data];
end

%%
params.Fs = 200;
params.fpass = [5 55];
params.tapers = [3 5];
[S, f]          = mtspectrumc(data_mat', params);

%%
for c1 = 5:55
    b_idx = find(f>=c1 & f <c1+1);
    S_bin(c1-4,:) = sum(S(b_idx,:));
end

%%
Sz = zscore(S_bin');
d1 = Sz(policy_mat == 0,:);
d2 = Sz(policy_mat == 1,:);
d3 = Sz(policy_mat == 2,:);

[a b c d e] = cross_val_classifier(d1, d2,5,1);
d
hold on
[a b c d e] = cross_val_classifier(d1, d3,5,2);
d



%%
openNSx('/Users/mconnolly/Intelligent Control Repository/workspace/raw_data/UG3_data/casper_data/casper_2018_05_15/casper_2018_05_15_005.ns2')

%%
d = double(NS2.Data);
for c1 = 1:1%size(d,1)
    df = bandpass(d(c1,:), [5 55], 1000, 'ImpulseResponse', 'iir');
    dr(c1,:) = resample(df,1,5);
end

%%
clc

plot((1:length(dr))/200, dr)
hold on
t_start     = [29.16, 682.7, 1321, 2005, 2567, 3159];
t_duty      = [20,    5,     5,    20,   20,   5];
offset      = 1.5;
for c1 = 2:size(t_start,2)
    if t_duty(c1) == 20
        pulse_int = 0:40:40*8;
    elseif t_duty(c1) == 5
        pulse_int = 0:10:10*31;
    end
    
    pulse_dur   = t_duty(c1);
    pulse_start = t_start(c1);
    pulse_start = pulse_start + pulse_int;
    pulse_end   = pulse_start + pulse_dur;

    pulse_start = pulse_start - offset;
    pulse_end   = pulse_end + offset;

    x_start     = [pulse_start;pulse_start];
    x_end       = [pulse_end;pulse_end];

    y           = [400 * ones(1,size(x_start,2)); 
                -400 * ones(1,size(x_start,2))];

    plot(x_start, y, 'r', 'LineWidth', 2);
    plot(x_end, y, 'k', 'LineWidth', 2);
    figure
    seg_end     = pulse_start(2:end) * 200;
    seg_start   = pulse_end(1:end-1) * 200;

    params.Fs       = 200;
    params.fpass    = [5 55];
    params.tapers   = [3 5];

    S_all           = [];
    d_segment       = [];
    for c2 = 1:size(seg_start,2)
        d_segment(c2,:) = dr(seg_start(c2):seg_end(c2));  
        [S, T, F]       = mtspecgramc(d_segment(c2,:), [1 .1], params);
        S_all           = [S_all; S];

    end
    
    stim_line = size(T,2):size(T,2):size(S_all,1)-1;
    x = [stim_line; stim_line];
    y = [5 * ones(1,size(x,2)); 55 * ones(1,size(x,2))];
    
    hold on   
    plot_matrix(S_all,1:size(S_all,1),F)
    plot(x,y, 'color', [1 1 1], 'LineWidth', 2)
end


