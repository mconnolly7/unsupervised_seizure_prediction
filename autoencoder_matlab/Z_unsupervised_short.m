% %%
clc
data_dir = 'workspace/raw_data/UG3_data/Waldo_data/Waldo_2017_11_29/';
data_file = {'007', '008', '009', '010'};
file_root = 'waldo11292017';
sampling_frequency  = 200;

for c1 = 2:2%size(data_file,2)
    ns2     = openNSx([data_dir file_root data_file{c1} '.ns2']);
    data_d  = double(ns2.Data(1:32,:));
    data_r  = [];
    for c2 = 1:1 %size(data_d,1)
        c2
        data_r(c2,:) = resample(data_d(c2,:),sampling_frequency, 1000);
    end
    
    data{c1} = data_r;
end


%%

index = 1;
for c1 = 1000:100:size(data,2)
    data_stream(index,:) = data(c1-999:c1);
    index = index+1;
end

%%
sz_table            = readtable('workspace/raw_data/UG3_data/Waldo_data/Waldo_2017_11_29/Waldo_1129107_Reconciled_2018_04_09.xlsx');
file_names          = unique(sz_table.File_Name);
window_size         = 5;

data_mat            = [];
time_mat            = [];
seizure_id_mat      = [];
sz_label_mat        = [];

for c1 = 1:size(data, 2)
    
    remainder       = mod(size(data{c1},2), sampling_frequency*window_size);
    data{c1}        = data{c1}(1:end-remainder);
    data{c1}        = (data{c1} - mean(data{c1})) / std(data{c1});
    data{c1}        = bandpass(data{c1},[5 55],200);
    
    recording_idx   = str2double(data_file{c1});  
    subtable        = sz_table(strcmp(sz_table.File_Name, file_names{c1}),:);
    
    t               = (1:size(data{c1},2))/sampling_frequency;
    sz_label        = zeros(size(t));
    
    for c2 = 1:size(subtable,1)
        sz_start_t      = subtable{c2,2};
        sz_end_t        = subtable{c2,3};
        
        sz_label_idx    = find(t > sz_start_t & t < sz_end_t);
        sz_label(sz_label_idx) = 1;
    end
    
    data_mat        = [data_mat reshape(data{c1}, sampling_frequency*window_size,[])];
    time_mat        = [time_mat reshape(t,sampling_frequency*window_size, [])];
    sz_label_mat    = [sz_label_mat reshape(sz_label', sampling_frequency*window_size, [])];

end

sz_label_mat        = mode(sz_label_mat,1);
time_mat            = mean(time_mat,1);

[a b c]             = zscore(data_mat);
data_mat(:,c>3)     = [];
sz_label_mat(c>3)   = [];

%%
sz_table = readtable('workspace/raw_data/UG3_data/Waldo_data/Waldo_2017_11_29/Waldo_seizure_data_2018_11_29.csv');
window_size         = 1;
offset              = .5;

sz_idx              = 1;
window_samples      = window_size * sampling_frequency;
data_mat            = [];
time_mat            = [];
seizure_id_mat      = [];
channel_idx_mat     = [];

for c1 = 1:size(data_file, 2)
    recording_idx   = str2double(data_file{c1});  
    subtable        = sz_table(sz_table.Var1 == recording_idx,:);
    
    t               = (1:size(data{c1},2))/sampling_frequency;
 
    for c2 = 1:size(subtable,1)
        if c2 < size(subtable,1)
            ii_start_idx        = subtable.Var3(c2) * sampling_frequency;
            ii_end_idx          = (subtable.Var2(c2+1) - offset) * sampling_frequency;
            interictal          = data{c1}(:,ii_start_idx:ii_end_idx);
            
            interictal_t        = t(ii_start_idx:ii_end_idx);
            segment_size        = ii_end_idx - ii_start_idx + 1;
            remainder_samples   = mod(segment_size, window_samples);
            
            interictal          = interictal(:,1:segment_size-remainder_samples);
            new_data            = reshape(interictal', window_samples,[]);
            data_mat            = [data_mat new_data];

            interictal_t        = (1:size(interictal,2))/sampling_frequency;
            interictal_t        = repmat(interictal_t, size(interictal,1),1);            
            time_mat            = [time_mat reshape(interictal_t', window_samples,[])];
            
            seizure_id_mat      = [seizure_id_mat repmat(sz_idx,1,size(new_data,2))];
            sz_idx              = sz_idx + 1;
            
            channel_idx         = repmat(1:size(interictal,1), size(new_data,2)/size(interictal,1),1);
            channel_idx_mat     = [channel_idx_mat; reshape(channel_idx,[],1)];
        end
    end
end

time_mat = median(time_mat,1);

%%
data_enc    = [];
layers      = [ 0 100 50 25 10 8 6 3 2];
% layers = [ 0 3 ];
% layers = [ 0 800 400 200 100 50 25 10 5 3 2];
clear d_n

data_enc{1} = zscore(data_mat(:,t_labels >= -1));
% data_enc{1} = zscore(data_mat);

for c1 = 2:size(layers,2)
    d_e                             = data_enc{c1 - 1};
    
    d_n{c1-1}                       = zscore(d_e); 
    
    auto_enc{c1-1}                  = trainAutoencoder(d_n{c1-1}, layers(c1));
    data_enc{c1}                    = encode(auto_enc{c1-1},d_n{c1-1});
    auto_enc_loss(c1)               = mse(d_n{c1-1} - predict(auto_enc{c1-1}, d_n{c1-1}))
end

%%

plot(layers(2:end), log(auto_enc_loss(2:end)))
set(gca, 'XDir', 'reverse')

%% Play movie
close all
clc
lag = 9;
d = data_enc{end-1};
for c1 = 10:size(d,2)
    
    subplot(2,1,1)
    for c2 = 1:lag
        scatter3(d(1,c1-c2), d(2,c1-c2), d(3,c1-c2), 50, [1 0 0]/c2,'filled')
        hold on
    end
    plot3(d(1,c1-lag:c1), d(2,c1-lag:c1), d(3,c1-lag:c1), 'color', 'k' )

    hold off
    xlim([0 .8])
    ylim([0 .8])
    zlim([0 .8])
    title(sprintf('Time: %.1fs', time_mat(c1)));
    
    subplot(2,1,2)
    
    a = data_mat(:,c1-lag:c1);

    plot(reshape(a,1,[]))
    drawnow
end

%% Plot trajectory
ii_start        = find(time_mat == time_mat(1));
ii_end          = ii_start(2:end) - 1;
label_window    = 3; % seconds

d               = data_mat;%data_enc{end-1};
t_labels        = -1*ones(size(time_mat));
for c1 = 1:size(ii_end,2)

    ii_middle_idx       = floor((ii_end(c1) + ii_start(c1)) / 2);
    
    ii_middle_start     = ii_middle_idx - label_window / window_size;
    ii_middle_end       = ii_middle_idx + label_window / window_size;
    
    ii_end_end          = ii_end(c1);
    ii_end_start        = ii_end(c1) - 2 * label_window /window_size;
    
    t_labels(ii_middle_start:ii_middle_end) = 0;
    t_labels(ii_end_start:ii_end_end) = 1;

end
scatter3(d(1,t_labels == 1),d(2,t_labels == 1),d(3,t_labels == 1))
hold on
scatter3(d(1,t_labels == 0),d(2,t_labels == 0),d(3,t_labels == 0))


%%
T           = t_labels(t_labels >=0);
X           = data_enc{1}(:,t_labels >=0);
cv_idx      = crossvalind('KFold', length(T), 5);

for c1 = 6:size(auto_enc,2)
    training_features   = X(:,cv_idx ~= 1);
    training_labels     = T(cv_idx ~= 1);

    test_features       = X(:,cv_idx == 1);
    test_labels         = T(cv_idx == 1);
    
    features            = data_enc{c1+1}(:,cv_idx ~= 1);

    softnet             = trainSoftmaxLayer(features,training_labels);
    deepnet             = stack(auto_enc{1:c1}, softnet);
    
    anova1(deepnet(test_features), test_labels)

    deepnet             = train(deepnet, training_features, training_labels);
%     anova1(deepnet(test_features), test_labels)
    
end

%%
gp_model = gp_object();
for c1 = 1:max(seizure_id_mat)
    idx                 = seizure_id_mat == c1 & channel_idx_mat' == 1;
    gp_model.initialize_data(time_mat(idx)', deepnet(data_enc{1}(:,idx))', min(time_mat(idx)), max(time_mat(idx)))
    sz_est              = gp_model.predict(time_mat(idx)');
    sz_trajectory(:,c1) = resample(sz_est, 1000, size(sz_est,1));
end

plot(time_mat(idx)',deepnet(data_enc{1}(:,idx)), 'color', .8*ones(3,1))
hold on
gp_model.initialize_data(time_mat(idx)', deepnet(data_enc{1}(:,idx))', min(time_mat(idx)), max(time_mat(idx)))
gp_model.minimize(1)
gp_model.plot_confidence_interval
gp_model.plot_mean

%%
m = mean(sz_trajectory,2);
s = std(sz_trajectory, [], 2);
ci = 1.96 * s / sqrt(size(sz_trajectory,2));

x = 1:size(sz_trajectory,1);
xx = [x flip(x)];
yy = [m+ci ;flip(m-ci)];

patch(xx,yy, .75*ones(1,3));
hold on
plot(x,m, 'color', 'k', 'LineWidth', 2);
xlim([2 995]);
xlabel('Scaled Time')
ylabel('Inter-ictal vs. Pre-ictal');
set(gca, 'FontSize', 16);
%%
T           = t_labels(t_labels >=0);

for c1 = 1:size(data_enc,2)
    dd = data_enc{c1};
    [X, Y, T, AUC, models] = cross_val_classifier(dd(:,T == 0)', dd(:,T == 1)', 5, 1)
end

%%
for c1 = size(data_enc,2):-1:1
    d = data_enc{c1};
    figure;
    [X, Y, T, AUC(c1,:), models] = cross_val_classifier(d(:,t_labels == 0)', d(:,t_labels == 1)', 5, 1);
end


%%

clear d_post d_pre
d_post(1,:) = reshape(d_interp_1(:,1:5), [],1);
d_post(2,:) = reshape(d_interp_2(:,1:5), [],1);
d_post(3,:) = reshape(d_interp_3(:,1:5), [],1);

% d_post(1,:) = reshape(d_interp_1(:,1:500), [],1);
% d_post(2,:) = reshape(d_interp_2(:,1:500), [],1);

d_pre(1,:) = reshape(d_interp_1(:,100:105), [],1);
d_pre(2,:) = reshape(d_interp_2(:,100:105), [],1);
d_pre(3,:) = reshape(d_interp_3(:,100:105), [],1);

scatter3(d_post(1,:), d_post(2,:), d_post(3,:))
hold on
scatter3(d_pre(1,:), d_pre(2,:), d_pre(3,:))


