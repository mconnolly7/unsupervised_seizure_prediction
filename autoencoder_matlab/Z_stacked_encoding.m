function X_enc = stacked_encoding(encoder, X)

data_enc_test{1} = X;
for c1 = 2:size(encoder,2)
    d_e             = data_enc_test{c1 - 1}';
    [d_n, a, b]     = zscore(d_e);
    
    data_enc_test{c1}   = encode(encoder{c1},d_n');
    
    X_enc = data_enc_test{end};
end
end

