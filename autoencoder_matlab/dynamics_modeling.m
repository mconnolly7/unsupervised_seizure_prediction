clear 

workspace_dir   = '/Users/mconnolly/Intelligent Control Repository/workspace/';
data_dir        = [workspace_dir 'raw_data/UG3_data/waldo_streaming_data/'];
processed_dir   = [workspace_dir 'processed_data/UG3_processed/waldo_streaming_processed/'];

processed_list  = dir([processed_dir '*.mat']);

xt0_mat         = [];
xt1_mat         = [];
dx_mat          = [];

label_mat       = [];
policy_mat      = [];

for c1 = 1:size(processed_list,1)
    processed_path  = [processed_dir processed_list(c1).name];
    data_path       = [data_dir processed_list(c1).name];
    
    encoded_data    = load(processed_path);
    meta_data       = load(data_path, 'label_data', 'policy_data', 'stim_idx_data');
    
    data_encoded    = encoded_data.encoded_data{end};
    data_encoded    = double(data_encoded);
    
    stim_indicies   = unique(meta_data.stim_idx_data);
    
    for c2 = 1:size(stim_indicies)
        stim_idx    = meta_data.stim_idx_data == stim_indicies(c2);

        data        = data_encoded(stim_idx,:);
        label       = meta_data.label_data(stim_idx);
        policy      = meta_data.policy_data(stim_idx);
        
        xt0         = data(1:end-1,:);
        xt1         = data(2:end,:);
        dx          = xt1 - xt0;
        
        xt0_mat     = [xt0_mat; xt0];
        xt1_mat     = [xt1_mat; xt1];
        dx_mat      = [dx_mat; dx];
        
        label_mat   = [label_mat; label(1:end-1)];
        policy_mat  = [policy_mat; policy(1:end-1)];
        
        scatter(xt0(:,3), dx(:,3))
        hold on
    end
    
    
    
end

%%
f = 2;
subplot(1,3,1)
scatter(xt0_mat(policy_mat==0,f), dx_mat(policy_mat==0,f));

subplot(1,3,2)
scatter(xt0_mat(policy_mat==1,f), dx_mat(policy_mat==1,f));

subplot(1,3,3)
scatter(xt0_mat(policy_mat==2,f), dx_mat(policy_mat==2,f));








