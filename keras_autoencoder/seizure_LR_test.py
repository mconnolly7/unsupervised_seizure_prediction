from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn import svm
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
from mpl_toolkits.mplot3d import Axes3D

data    = np.load('encoded_data.npz')
X_hat   = data['data_enc']
X_hat   = X_hat[-1]
print(X_hat.shape)

# exit()
data_struct = sio.loadmat('./data/sz_data-200Hz-5s_window_norm_clean.mat')
Y           = data_struct['sz_label_mat'].T
X           = data_struct['data_mat'].T
# plt.plot(X[286])
# plt.show()
Y = np.squeeze(Y)
np.savetxt('encoded_Y.csv', Y, delimiter=',')

print(X.shape)
print(Y.shape)

x_train, x_test, y_train, y_test = train_test_split(X_hat,Y, test_size=0.25)
print(x_train.shape)

# all parameters not specified are set to their defaults
plt.figure(1)

clf = svm.SVC(kernel='linear')
clf.fit(x_train, y_train)
trace           = clf.predict(X_hat)
# plt.plot(trace)
# plt.plot(Y.T)
# plt.show()

# plt.scatter(X_hat[Y == 0, 1], X_hat[Y == 0, 2])
# plt.scatter(X_hat[Y == 1, 1], X_hat[Y == 1, 2])

post    =  X_hat[1:-1,0]
pre     =  X_hat[0:-2,0]

from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, WhiteKernel

plt.scatter(pre, post-pre)
plt.show()
exit()
for c1 in range(5,Y.shape[0]):

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(X_hat[Y == 0, 0], X_hat[Y == 0, 1],X_hat[Y == 0, 2])
    ax.scatter(X_hat[Y == 1, 0], X_hat[Y == 1, 1], X_hat[Y == 1, 2])

    idx = np.arange(c1-5,c1+1)
    ax.plot(X_hat[idx,0],X_hat[idx,1],X_hat[idx,2], color='red')
    ax.scatter(X_hat[c1,0],X_hat[c1,1],X_hat[c1,2], color='black')
    plt.show()


predictions     = clf.predict(x_test)

score               = clf.score(x_test, y_test)
plt.plot(score)
fpr, tpr, _         = metrics.roc_curve(y_test, predictions)

plt.figure(2)
plt.plot(fpr,tpr)
print(score)

plt.show()
