from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C

import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio

data    = np.load('encoded_data.npz')
X_hat   = data['data_enc']
X_hat   = X_hat[-1]

# np.savetxt('encoded_X.mat', X_hat, delimiter=',')
post    =  np.expand_dims(X_hat[1:-1,0], axis=1)
pre     =  np.expand_dims(X_hat[0:-2,0], axis=1)

plt.scatter(pre, post-pre)

gp = GaussianProcessRegressor(n_restarts_optimizer=9)

# Fit to data using Maximum Likelihood Estimation of the parameters
gp.fit(pre, post-pre)

x       = np.linspace(np.min(pre), np.max(pre), 100)
x       =  np.expand_dims(x, axis=1)
y       = gp.predict(x)


x_est       = np.zeros_like(post)
x_est[0]    = pre[0]

for c1 in np.arange(1, x_est.shape[0]):
    plt.subplot(2, 1, 1)
    plt.plot(x, y)

    delta_x         = gp.predict(x_est[c1-1,0])
    print(x_est[c1-1,0])
    print(delta_x)
    print(c1)
    print('-----')
    x_est[c1,0]     = x_est[c1-1,0] + delta_x

    plt.subplot(2,1,2)
    plt.plot(x_est)
    plt.show()