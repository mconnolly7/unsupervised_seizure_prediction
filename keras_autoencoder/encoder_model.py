import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

from scipy import stats
from utils import *
from os import listdir
from keras.models import load_model


def encode_helper(file_name, workspace_dir):

    model_path      = '/Users/mconnolly/PycharmProjects/keras_autoencoder/models/stacked_AE_2018_05_06'
    data_dir        = workspace_dir + 'raw_data/UG3_data/waldo_streaming_data/'
    save_dir        = workspace_dir + 'processed_data/UG3_processed/waldo_streaming_processed/'

    data_file       = data_dir + file_name
    save_file       = save_dir + file_name
    data_struct     = sio.loadmat(data_file)

    X_data          = data_struct['segmented_data']

    encoder_layer   = np.empty((11,), dtype=object)
    encoded_data    = np.empty((11,), dtype=object)
    encoded_data[0] = X_data

    for l in range(1,11):
        encoder_layer_path  = '%s/encoder_model-L%d' % (model_path, l)
        encoder_layer[l-1]  = load_model(encoder_layer_path)

        d_e                 = encoded_data[l-1]
        d_e                 = stats.zscore(d_e)
        d_e, _              = scale_data(d_e, 0, 1)
        encoded_data[l]     = encoder_layer[l-1].predict(d_e)

    encoded_data = encoded_data[5:]

    mdict = {}
    mdict['encoded_data']   = encoded_data

    sio.savemat(save_file, mdict)


workspace_dir   = '/Users/mconnolly/Intelligent Control Repository/workspace/'
data_dir        = workspace_dir + 'raw_data/UG3_data/waldo_streaming_data/'

d = listdir(data_dir)

for data_file in d:
    encode_helper(data_file, workspace_dir)

