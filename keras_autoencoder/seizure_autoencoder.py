from keras.layers import Input, Dense, Activation
from keras.models import Model
from keras.layers.normalization import BatchNormalization

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np

input_dim   = 1000
# layers_dim  = [2000]
layers_dim  = [2000, 1000, 500, 250, 100, 80, 60, 30, 10, 3]
latent_dim  = 10
decode_idx  = len(layers_dim) + 2

# Build encoder
X       = Input(shape=(input_dim,))
x_e     = X
for l in layers_dim:
    x_e = Dense(l, activation='sigmoid')(x_e)

latent  = Dense(latent_dim, activation=None)(x_e)
latent  = Activation('sigmoid')(latent)

# latent   = Dense(latent_dim, activation='relu')(x_e)
encoder  = Model(inputs=X, outputs=latent)

# Build decoder
latent_inputs   = Input(shape=(latent_dim,))

x_d = latent_inputs
for l in layers_dim[::-1]:
    x_d = Dense(l, activation='sigmoid')(x_d)

decoded = Dense(input_dim, activation='sigmoid')(x_d)
decoder = Model(inputs=latent_inputs, outputs=decoded)

# Build autoencoder
BN = BatchNormalization()(encoder(X))
autoencoder = Model(inputs=X, outputs=decoder(BN))

autoencoder.compile(optimizer='adadelta', loss='mse')

# Load the data
data_struct = sio.loadmat('./data/sz_data-200Hz-5s_window_norm_clean.mat')
X           = data_struct['data_mat'].T

scaler      = MinMaxScaler(feature_range=(0, 1))
scaler.fit(X)
X           = scaler.transform(X)
#
# X           = (X - np.mean(X, axis=1, keepdims=True)) / np.std(X,axis=1, keepdims=True) / 2
Y           = data_struct['sz_label_mat'].T

x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size=0.25)

autoencoder.fit(x_train, x_train,
                epochs=5,
                batch_size=64,
                shuffle=True,
                validation_data=(x_test, x_test))

# encode and decode some digits
# note that we take them from the *test* set
encoded_imgs = encoder.predict(x_test)
decoded_imgs = decoder.predict(encoded_imgs)

# use Matplotlib (don't ask)
plt.figure(1)
n = 10  # how many digits we will display
for i in range(n):
    # display original
    ax = plt.subplot(n, 1, i + 1)
    plt.plot(x_test[i])
    plt.plot(decoded_imgs[i])


plt.figure(2)
plt.plot(encoded_imgs)
plt.show()