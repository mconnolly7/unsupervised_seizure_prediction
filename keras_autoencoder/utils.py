from sklearn.preprocessing import MinMaxScaler


def scale_data(X, min, max):
    scaler      = MinMaxScaler(feature_range=(min, max))
    scaler.fit(X)
    X_scaled    = scaler.transform(X)

    return X_scaled, scaler