from keras.layers import Input, Dense
from keras.models import Model
from keras import regularizers
from keras import optimizers
from utils import *
from scipy import stats
import os
import scipy.io as sio

import matplotlib.pyplot as plt
import numpy as np


# TODO change arguments to just the data
def sae_helper(data_file, save_file, model_dir):

    EPOCHS          = 1000
    L1_REG          = 0e-5
    L2_REG          = 0e-3
    LEARNING_RATE   = 0.001

    MODEL_SAVE_DIR  = model_dir

    input_dim       = 1000
    LAYERS_DIM      = [input_dim, 2000, 1000, 500, 250, 100, 80, 60, 30, 10, 3]
    ACTIVATION      = 'sigmoid'
    BATCH_NORM      = False
    n_layers        = len(LAYERS_DIM)

    # Load the data
    data_struct         = sio.loadmat(data_file)
    X_data              = data_struct['segmented_data']

    encoded_data        = np.empty(shape=(n_layers,), dtype=object)
    data_recon          = np.empty(shape=(n_layers,), dtype=object)

    encoded_data[0]     = X_data

    #
    # Stacked AE loop
    #
    for l in range(1,n_layers):

        X       = Input(shape=(LAYERS_DIM[l-1],))

        x_e     = Dense(LAYERS_DIM[l], activation=ACTIVATION,
                    activity_regularizer=regularizers.l1(L1_REG),
                    kernel_regularizer=regularizers.l2(L2_REG))(X)

        x_d     = Dense(LAYERS_DIM[l-1], activation=ACTIVATION,
                    activity_regularizer=regularizers.l1(L1_REG),
                    kernel_regularizer=regularizers.l2(L2_REG))(x_e)

        autoencoder = Model(inputs=X, outputs=x_d)
        encoder     = Model(inputs=X, outputs=x_e)

        d_e         = encoded_data[l-1]
        d_e         = stats.zscore(d_e)
        d_e, _      = scale_data(d_e, 0, 1)

        adam_opt    = optimizers.adam(lr=LEARNING_RATE)

        autoencoder.compile(optimizer=adam_opt, loss='mean_squared_error')
        autoencoder.fit(d_e, d_e,
                        epochs=(EPOCHS if isinstance(EPOCHS, int) else EPOCHS[l-1]),
                        batch_size=64,
                        shuffle=False,
                        verbose=2)

        data_recon[l]   = autoencoder.predict(encoded_data[l-1])
        encoded_data[l]     = encoder.predict(encoded_data[l-1])

        autoencoder.save('%s/autoencoder_model-L%d' % (MODEL_SAVE_DIR, l))
        encoder.save('%s/encoder_model-L%d' % (MODEL_SAVE_DIR, l))

    mdict = {}
    mdict['encoded_data'] = encoded_data[5:]
    sio.savemat(save_file, mdict)


#####
workspace_dir   = '/Users/mconnolly/Intelligent Control Repository/workspace/'
data_dir        = workspace_dir + 'raw_data/UG3_data/waldo_window_data/'
save_dir        = workspace_dir + 'processed_data/UG3_processed/waldo_window_processed/'

d = os.listdir(data_dir)

for file_name in d:
    data_file = data_dir + file_name
    save_file = save_dir + file_name
    model_dir = 'models/' + file_name[:-4] + '/'
    # os.makedirs(model_dir, exist_ok=True)
    sae_helper(data_file, save_file, model_dir)

