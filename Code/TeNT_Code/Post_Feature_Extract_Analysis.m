%Indexing full 30 second feature set
n = 1;
n_files = 10;

w_samples = (((0.5/0.01)*n)+1)*n_files;
n_samples = 50;

X_ii_w1 = X_ii(1:w_samples,:);
X_pi_w1 = X_pi(1:w_samples,:);

rand_idx = randperm(size(X_ii_w1,1),n_samples);
rand_idx = rand_idx';

%sub-sampled features for window size
X_pi_w1_sampled = X_pi_w1(rand_idx,:);
X_ii_w1_sampled = X_ii_w1(rand_idx,:);

%label vectors
Y_pi = ones(size(X_pi_w1_sampled,1),1);
Y_ii = zeros(size(X_ii_w1_sampled,1),1);

%(X,Y) input for classifier
X = vertcat(X_pi_w1_sampled, X_ii_w1_sampled);
Y = vertcat(Y_pi, Y_ii);

