%% Interictal Window
function [D, interictal_idx_1, interictal_idx_2] = Interictal_Window_Cell(sz_start, data, fs, w)

for idx = 1
interictal_start = ceil(sz_start/2);

interictal_idx_1 = interictal_start - (w/2)*fs*(idx);
interictal_idx_2 = interictal_start + (w/2)*fs*(idx);

interictal_window = data(interictal_idx_1:interictal_idx_2);

%t_interictal = (1:length(interictal_window));

%patch_interictal = patch([interictal_idx_1,interictal_idx_2,interictal_idx_2,interictal_idx_1],[-800,-800,800,800],'k');
%hold on
end

D = interictal_window;
end

