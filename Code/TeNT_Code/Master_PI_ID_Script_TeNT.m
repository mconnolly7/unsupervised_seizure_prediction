%% Data Load
clear

data_load = load('/Users/Hith/Box_Sync/Honors_Thesis/Data/TeNT_Data_Cell.mat');
data_all = data_load.table;

%select data (for individual animals)
data_all = data_all(1:10,:);

%randomize data (n = number of desired files)
% n = 20;
% 
% data_rand_idx = randperm(size(data_all,1),n);
% data_all = data_all(data_rand_idx,:);

%% Pre-Ictal Spectra

w = 1;

X_pi = [ ];

for idx = 1:size(data_all,1)
    % Cycle Through Cell
    [data, sz_start, fs, w] = Seizure_Cell_Extract(data_all, idx, w);
    
    % Window Data
    [D] = Pre_Seizure_Window_Cell(sz_start, data, fs, w);
    
    % Extract Spectral Features
    [X_delta, X_theta, X_alpha, X_beta, X_gamma, S] = Data_Band_Extract_Function_Cell(D);
    
    
    X_single = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);
    
    X_pi       = vertcat(X_pi, X_single);
end

%Y_pi = ones(500,1);
%Y_ii = zeros(500,1);

Y_pi = ones(size(X_pi,1),1);
Y_ii = zeros(size(X_pi,1),1);


% %% Concatenate for One Seizure
% X1_all = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);
%
% %% Combine Features Across Seizures
% %X_pi      = vertcat(X1_all, X2_all);
% X_pi       = vertcat(X_pi, X3_all);

%% Interictal Spectra

w = 1;

X_ii = [ ];

for idx = 1:size(data_all,1)
    % Cycle Through Struct
    [data, sz_start, fs, w] = Seizure_Cell_Extract(data_all, idx, w);
    
    % Window Data
    [D] = Interictal_Window_Cell(sz_start, data, fs, w);
    
    % Extract Spectral Features
    [X_delta, X_theta, X_alpha, X_beta, X_gamma, S] = Data_Band_Extract_Function_Cell(D);
    
    
    X_single = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);
    
    X_ii       = vertcat(X_ii, X_single);
end

X = vertcat(X_pi,X_ii);
Y = vertcat(Y_pi,Y_ii);



%spectral_band.window30_X = X;
%spectral_band_no_sample.window1 = X;

%% Logistic Regression
%[X_roc, Y_roc, AUC_roc] = Logistic_Regression_Val(X,Y);