%% Pre-Seizure Window
function [D] = Pre_Seizure_Window_Cell(sz_start, data, fs, w)

for idx = 1
sz_start = ceil(sz_start);
pre_sz_start = sz_start - w*fs*(idx);

pre_sz_window = data(pre_sz_start:sz_start);


%t_pre_sz = (1:length(pre_sz_window));

%patch_pre_sz = patch([pre_sz_start,sz_start,sz_start,pre_sz_start],[-800,-800,800,800],'m','FaceAlpha',0.5);
%hold on
end

D = pre_sz_window;

end