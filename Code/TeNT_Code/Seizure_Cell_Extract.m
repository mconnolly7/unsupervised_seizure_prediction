%% Load Cell Data
function [data, sz_start, fs, w] = Seizure_Cell_Extract(data_all, idx, w)

data_struct = load(data_all{idx});

data = data_struct.data;
data = data';
data = data(:,1);

sz_start = data_struct.Seiz_onset;

fs = 2000;

end