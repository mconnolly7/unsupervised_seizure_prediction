%% 

data = seizure(2).lfp;
sz_start = seizure(2).start - seizure(1).end;
fs = 1000;

%% Pre-Seizure Window
w2 = 5;

for idx = 1
sz_start = floor(sz_start);
pre_sz_start = sz_start - w2*(idx);

% pre_sz_window = data(pre_sz_start:sz_start);
pre_sz_window = data(fs*pre_sz_start:fs*sz_start);

t_pre_sz = (1:length(pre_sz_window));

%patch_pre_sz = patch([pre_sz_start,sz_start,sz_start,pre_sz_start],[-800,-800,800,800],'k');
%hold on
end

%% Interictal Window
w1 = 10;

for idx = 1
interictal_start = floor(sz_start/2);

interictal_idx_1 = interictal_start - w1*(idx);
interictal_idx_2 = interictal_start + w1*(idx);

%interictal_window = data(interictal_idx_1:interictal_idx_2);
interictal_window = data(fs*interictal_idx_1:fs*interictal_idx_2);


t_interictal = (1:length(interictal_window));

%patch_interictal = patch([interictal_idx_1,interictal_idx_2,interictal_idx_2,interictal_idx_1],[-800,-800,800,800],'k');
%hold on
end

%% PSD using Chronux
params.Fs = 1000;
params.tapers = [3 5];
[S, f] = mtspectrumc(pre_sz_window, params);
plot(f,log(S));
xlim([0 100]);
xlabel('Frequency (Hz)')
%ylabel('Power/Freq(dB/Hz)')
ylabel('dB')
title('PSD - MTM')

%% Spectrogram using Chronux
[S, t, f] = mtspecgramc(pre_sz_window, [1 1], params);
plot_matrix(S,t,f)
ylim([0 150])
xlabel('Frequency (Hz)')
ylabel('Frequency (Hz)')
xlabel('Time (s)')
title('Spectrogram Using Chronux Multi-Taper')