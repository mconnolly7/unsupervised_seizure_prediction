%% Pre-Ictal Spectra


load('/Users/Hith/Documents/Beckman_Summer_2019/Data/seizure_structure.mat')

w = 1;

X_pi = [ ];

for idx = 1:10
% Cycle Through Struct
[data, sz_start, fs, w] = Seizure_Struct_Extract(seizure, idx, w);

% Window Data
[D] = Pre_Seizure_Window(sz_start, data, fs, w);

% Extract Spectral Features
[X_delta, X_theta, X_alpha, X_beta, X_gamma, S] = Data_Band_Extract_Function(D);


X_single = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);

X_pi     = vertcat(X_pi, X_single);
end

%Y_pi = ones(500,1);
%Y_ii = zeros(500,1);

Y_pi = ones(size(X_pi,1),1);
Y_ii = zeros(size(X_pi,1),1);


% %% Concatenate for One Seizure
% X1_all = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);
% 
% %% Combine Features Across Seizures
% %X_pi      = vertcat(X1_all, X2_all);
% X_pi       = vertcat(X_pi, X3_all);

%% Interictal Spectra
w = 1;

X_ii = [ ];

for idx = 1:10
% Cycle Through Struct
[data, sz_start, fs, w] = Seizure_Struct_Extract(seizure, idx, w);

% Window Data
[D] = Interictal_Window(sz_start, data, fs, w);

% Extract Spectral Features
[X_delta, X_theta, X_alpha, X_beta, X_gamma, S] = Data_Band_Extract_Function(D);


X_single = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);

X_ii     = vertcat(X_ii, X_single);
end

X = vertcat(X_pi,X_ii);
Y = vertcat(Y_pi,Y_ii);

%spectral_band.window30_X = X;
spectral_band_no_sample.window1 = X;

%% Logistic Regression
function [X_roc, Y_roc, AUC_roc] = Logistic_Regression_Val(X,Y)

cv_idx = crossvalind('Kfold',size(X,1),5);

for c1 = 1:max(cv_idx)
    
    train_idx = cv_idx ~= c1;
    test_idx  = cv_idx == c1;
    
    % X is my vector of extracted features; Y is my vector of labels
    X_train = X(train_idx,:);
    Y_train = Y(train_idx,:);
    
    X_test = X(test_idx,:);
    Y_test = Y(test_idx,:);
    
    b     = glmfit(X_train,Y_train,'binomial','link','logit');
    Y_hat = glmval(b,X_test,'logit');
    
    % Label
    y_all_test{c1} = Y_test; 
    
    % Ground Truth Estimate
    y_all_hat{c1}  = Y_hat;
    
end

[X_roc,Y_roc,~,AUC_roc] = perfcurve(y_all_test,y_all_hat,1);

%figure
%plot(X_roc,Y_roc)

    
%print('/Users/Hith/Documents/SURE 2018/Poster/Figures/Figure 1: Color_Plot_Gait_Final','-dpng','-r500')


end