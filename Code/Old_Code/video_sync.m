%%
experiment_name = 'waldo_2017_11_29_010';
%v = Videoreader( ['PCN_Seizure/01112018/' experiment_name '.mpg']);
%openNSx(['PCN_Seizure/Raw_Data/Waldo_Data/11292017_New/' experiment_name '.ns2'])
openNSx(['Data/11292017_New/' experiment_name '.ns2'])
%%
fs = NS2.MetaTags.SamplingFreq;

data = double(NS2.Data(1,:));
%%
highpass_filt = designfilt('highpassiir','FilterOrder',4, ...
      'HalfPowerFrequency',5, ...
      'DesignMethod','butter','SampleRate',1000);

notch_filt = designfilt('bandstopiir','FilterOrder',4, ...
      'HalfPowerFrequency1',55, 'HalfPowerFrequency2',65, ...
      'DesignMethod','butter','SampleRate',1000);

hp_data = filtfilt(highpass_filt,data');
data    = filtfilt(notch_filt,hp_data);


%plot(NS2.data{1}(1,:))

%plot(((1:length(data))/fs),data)

t_all = (1:length(data))/fs;

%figure
%plot(t_all,data)
%%
v.CurrentTime = 0;
figure
while hasFrame(v)
    t = v.CurrentTime;
    if t > 5
        start_idx = (t-5)*fs;
        end_idx   = t*fs;
        tt = (start_idx:end_idx)/fs;
        subplot(2,1,1)
        plot(tt, data(int32(start_idx): int32(end_idx)));
        ylim([-700 700]);
        xlim([tt(1) tt(end)]); 
    end 
    
    subplot(2,1,2)
    vidFrame = readFrame(v);
    
    image(vidFrame);
    pause(1/v.FrameRate/2);
    
end




    
