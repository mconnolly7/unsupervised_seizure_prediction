% %load data
% data = openNSx('datafile002.ns2');
% D = data.Data(1,:);
%% set parameters
D = pre_sz_window;
params.Fs = 1000;
params.tapers = [3 5];
%params.fpass = [4 8];

%% PSD
[S,f] = mtspectrumc(D,params);
figure
plot(f,S)

%% Spectrogram
[S,t,f] = mtspecgramc(D,[1 .1],params);
figure
plot_matrix(S,t,f)

%% Delta Spectral Feature
D = pre_sz_window;
params.Fs = 1000;
params.tapers = [3 5];
params.fpass = [0 4];

[S,t,f] = mtspecgramc(D,[1 .1],params);

s = mean(S,2);

rand_idx = randi(size(s,1),25,1);

X_delta = s(rand_idx,1);
%% Theta Spectral Feature
D = pre_sz_window;
params.Fs = 1000;
params.tapers = [3 5];
params.fpass = [4 8];

[S,t,f] = mtspecgramc(D,[1 .1],params);

s = mean(S,2);

rand_idx = randi(size(s,1),25,1);

X_theta = s(rand_idx,1);
%% Alpha Spectral Feature
D = pre_sz_window;
params.Fs = 1000;
params.tapers = [3 5];
params.fpass = [8 13];

[S,t,f] = mtspecgramc(D,[1 .1],params);

s = mean(S,2);

rand_idx = randi(size(s,1),25,1);

X_alpha = s(rand_idx,1);
%% Beta Spectral Feature
D = pre_sz_window;
params.Fs = 1000;
params.tapers = [3 5];
params.fpass = [13 32];

[S,t,f] = mtspecgramc(D,[1 .1],params);

s = mean(S,2);

rand_idx = randi(size(s,1),25,1);

X_beta = s(rand_idx,1);
%% Gamma Spectral Feature
D = pre_sz_window;
params.Fs = 1000;
params.tapers = [3 5];
params.fpass = [32 60];

[S,t,f] = mtspecgramc(D,[1 .1],params);

s = mean(S,2);

rand_idx = randi(size(s,1),25,1);

X_gamma = s(rand_idx,1);
%% Combine Features for One Seizure
X1_all = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);

% idx = 2;
%data_band_window_5(idx).pi = X1_all;2

%% Combine Features Across Seizures
%X_pi       = vertcat(X1_all, X2_all);
X_pi       = vertcat(X_pi, X3_all);


