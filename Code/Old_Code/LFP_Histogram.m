%% Create Logical Vector for Seizure/No Seizure %dog
sz_annotations           = xlsread('waldo_2017_11_29_seizure_table');

%Specify seizure entry number for recording
sz_annotations_day = sz_annotations(1:18,:);


%Choose what column from spreadsheet is start and end
sz_start                 = sz_annotations_day(:,1);
sz_end                   = sz_annotations_day(:,2);

%Initialize zeros matrix and time matrix
recording_matrix         = zeros(1,size(t_all,2));
t_rec                    = (1:size(recording_matrix,2))/fs;


%Loop through seizure start and stop
for seizure_idx = 1:size(sz_annotations_day,1)
    recording_matrix(seizure_idx,:) = (t_rec >= sz_start(seizure_idx) & t_rec <= sz_end(seizure_idx));
end


close all


%Sum all rows to get one logical vector with seizure (1) and no seizure (0)
all_seizure = sum(recording_matrix);

%Use diff function to specify seizure start (1) and  seizure end (-1)
dx_seizure  = diff(all_seizure);

%% ictal duration
sz_start    = find(dx_seizure == 1) + 1;
sz_end      = find(dx_seizure <  0) + 1;

%Difference of start and end indexes divided by fs will give ictal duration
ictal       = (sz_end - sz_start)/fs;
figure
histogram(ictal,'BinWidth',20)

% xlabel('Time (s)')
% ylabel('Frquency')
% title('Waldo 11/29/2017 - Rec 8 - Ictal Duration')
% print(gcf,'-bestfit','/Users/Hith/Documents/Beckman_Summer_2019/Figures/Meeting_Figures/Waldo_2017_11_29_007_Ictal_Duration','-dpdf')

%% interictal duration
Ii_start    = find(dx_seizure == 1) + 1;
Ii_end      = find(dx_seizure <  0) + 1;
Ii_start(1) = [];
Ii_end(end) = [];

%Difference of start and end indexes divided by fs will give interictal duration
interictal  = (Ii_start - Ii_end)/fs;

hold on
histogram(interictal,'BinWidth',20)

xlabel('Time (s)')
ylabel('Frequency')
title('Waldo 11/29/2017 - Rec 9 - Interictal + Ictal Duration')

print(gcf,'-bestfit','/Users/Hith/Documents/Beckman_Summer_2019/Figures/Meeting_Figures/Waldo_2017_11_29_007_InterIctal_Duration','-dpdf')