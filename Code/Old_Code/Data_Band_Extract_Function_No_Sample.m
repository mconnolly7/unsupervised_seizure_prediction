%NO SUB_SAMPLING
function [X_delta, X_theta, X_alpha, X_beta, X_gamma, S] = Data_Band_Extract_Function_No_Sample(D)
window = 0.5;
window_step = 0.01;

%Params
params.Fs = 1000;
params.tapers = [3 5];

% Delta Spectral Feature
params.fpass = [0 4];

[S,t,f] = mtspecgramc(D,[window window_step],params);
s = mean(S,2);

X_delta = s;

% Theta Spectral Feature
params.fpass = [4 8];

[S,t,f] = mtspecgramc(D,[window window_step],params);
s = mean(S,2);



X_theta = s;

% Alpha Spectral Feature

params.fpass = [8 13];

[S,t,f] = mtspecgramc(D,[window window_step],params);
s = mean(S,2);



X_alpha = s;

% Beta Spectral Feature
params.fpass = [13 32];

[S,t,f] = mtspecgramc(D,[window window_step],params);
s = mean(S,2);


X_beta = s; 

% Gamma Spectral Feature
params.fpass = [32 60];

[S,t,f] = mtspecgramc(D,[window window_step],params);
s = mean(S,2);


X_gamma = s;

end