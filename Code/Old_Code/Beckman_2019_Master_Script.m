%% Extract Spectra
load('/Users/Hith/Box_Sync/Honors_Thesis/Data/NHP_Data/KP_seizure_struct.mat')

w = 1;

X_pi = [ ];

for idx = 1:8
% Cycle Through Struct
[data, sz_start, fs, w] = Seizure_Struct_Extract(seizure, idx, w);

% Window Data
[D] = Pre_Seizure_Window(sz_start, data, fs, w);

% Extract Spectral Features
[X_delta, X_theta, X_alpha, X_beta, X_gamma, S] = Data_Band_Extract_Function(D);


X_single = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);

X_pi       = vertcat(X_pi, X_single);
end

%Y_pi = ones(500,1);
%Y_ii = zeros(500,1);

Y_pi = ones(size(X_pi,1),1);
Y_ii = zeros(size(X_pi,1),1);


% %% Concatenate for One Seizure
% X1_all = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);
% 
% %% Combine Features Across Seizures
% %X_pi      = vertcat(X1_all, X2_all);
% X_pi       = vertcat(X_pi, X3_all);