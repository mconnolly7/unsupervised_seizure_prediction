
for idx           = 1:size(seizure,2)
    data          = seizure(idx).ictal;
    params.Fs     = 1000;
    params.tapers = [3 5];
    [S, f]        = mtspectrumc(data, params);
    S(:,idx) = S;
end 