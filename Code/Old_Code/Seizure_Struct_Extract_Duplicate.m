function [sz_start, fs] = Seizure_Struct_Extract_Duplicate(seizure_2)
for idx = 1:7
data = seizure_2(idx).lfp;

if idx == 1
sz_start = seizure_2(idx).start
else
    sz_start = seizure_2(idx).start - seizure_2(idx-1).end
end

fs = 2000;

end
end


