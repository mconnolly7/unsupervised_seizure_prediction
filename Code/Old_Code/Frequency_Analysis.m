%% FFT
N = length(data);
FFT = fft(data);
FFT = FFT(1:N/2+1);

power_spec_dx = (1/(fs*N)) * abs(FFT).^2;
power_spec_dx(2:end-1) = 2*power_spec_dx(2:end-1);

freq = 0:fs/length(data):fs/2;

plot(freq,10*log10(power_spec_dx))
xlim([0 100])

grid on

title('Periodogram Using FFT')
xlabel('Frequency (Hz)')
ylabel('Power/Freq(dB/Hz)')
title('PSD Using FFT')

%% Pwelch - 1st Plot
N = length(data);
window = rectwin(N);
[pxx,f]= pwelch(data,window,0,N,fs);
PdB_Hz= 10*log10(pxx);

figure
plot(f,PdB_Hz)
xlim([0 100])
xlabel('Frequency (Hz)')
ylabel('Power/Freq(dB/Hz)')

title('PSD Using Rectangular Window Pwelch')

%% Pwelch - 2nd Plot
[pxx, f] = pwelch(data,hanning(1000),0,[],fs);
plot(f,10*log10(pxx))

xlim([0 100])
xlabel('Frequency (Hz)')
ylabel('Power/Freq(dB/Hz)')

title('PSD Using Pwelch Hanning')

%% PSD using Chronux
params.Fs = 1000;
params.tapers = [3 5];
[S, f] = mtspectrumc(data, params);
plot(f,log(S));
xlim([0 100]);
xlabel('Frequency (Hz)')
ylabel('Power/Freq(dB/Hz)')
title('PSD Using Chronux Multi-Taper')

%% Spectrogram using Chronux
[S, t, f] = mtspecgramc(data, [1 1], params);
plot_matrix(S,t,f)
ylim([0 150])
xlabel('Frequency (Hz)')
ylabel('Frequency (Hz)')
xlabel('Time (s)')
title('Spectrogram Using Chronux Multi-Taper')
