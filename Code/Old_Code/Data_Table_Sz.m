data_table = [];
data_path  = '/Users/Hith/Box_Sync/Honors_Thesis/Data/TeNT_Data/';
d1         = dir(data_path);
%lfp_time   = (1:length(data))/fs;
fs         = 2000;

for c1 = 4:size(d1,1)
   
    dir_name = d1(c1).name;
    dir_path = [data_path dir_name];
    d2       = dir(dir_path);
    
    for c2 = 4:size(d2,1)
        
        file_name = d2(c2).name;
        load(file_name);
        
        final_path = {[dir_path '/' file_name]};
        
        date = {[date]};
        Animal_ID = {[Animal_ID]};
        
        table_row  = table(final_path, fs, Seiz_onset, Seiz_termin, date, Animal_ID);
        data_table = [data_table; table_row];
      
    end
end

%% Single Loop

data_table = [];
data_path  = '/Users/Hith/Box_Sync/Honors_Thesis/Data/Test_Data_TeNT/';

d1         = dir(data_path);

%lfp_time   = (1:length(data))/fs;
fs         = 2000;

for c1 = 3:size(d1,1)
   
    file_name = d1(c1).name;
  
    load(file_name);
    
    final_path = {[data_path file_name]};
        
    date = {[date]};
    Animal_ID = {[Animal_ID]};
        
    table_row  = table(final_path, fs, Seiz_onset, Seiz_termin, date, Animal_ID);
    data_table = [data_table; table_row];
      
end

