%% Load Gait Data
load('FES09.mat');
D1 = getJointAngles(TrialData(1));
D2 = getJointAngles(TrialData(2));
D  = (cat(1,D1,D2));
fs = 100;
window_sizes = [5 10 20];

%% Load Seizure Data
load('biomarker_dynamics_data.mat');
D = zscore((data_band)');
D = [D(1:end-1,:) (D(2:end,:)-D(1:end-1,:))];
fs = 10;
window_sizes = [10 50 100];

%% K-means Analysis: Cluster Sweep 

for c2 = 1:size(window_sizes,2)
    window = window_sizes(c2);
    
    r = mod(size(D,1),window); 
    D = D(1:end-r,:);
    
    D_mat = [];
    for c1 = 1:size(D,2)
        d_mat1 = (reshape(D(:,c1),window,[]))';
        D_mat  = [D_mat d_mat1];
    end
    
  
    for c1 = 1:10
        [idx, C, sumD, F] = kmeans(D_mat,c1);
        s(c1,c2) = sum(sumD);
        %s(c1,c2) = sum(sumD/sqrt(size(D_mat,2)));
        %s(c1,c2) = mean(F/sqrt(size(D_mat,2)));
        %n_dim = size(D_mat,2);
        %n_obs = size(D_mat,1);
        %s(c1,c2) = sum(sumD/((sqrt(n_dim))*(n_obs)));
    end
end


figure
plot(s)
legend('Window 10','Window 50','Window 100')
title('KMeans Test')
ylabel('Within-Cluster Sums of Point-to-Centroid Distances (SumD)')
xlabel('Number of Clusters')

%% K-means Analysis with "Ideal" HyperParameters

D_mat = [];
optimal_window = 50;
r = mod(size(D,1),optimal_window);
D = D(1:end-r,:);
    
for c1 = 1:size(D,2)
    
    d_mat1 = (reshape(D(:,c1),optimal_window,[]))';
    D_mat  = [D_mat d_mat1];
end

%kmeans
k = 4;
[idx, centroids] = kmeans(D_mat,k);
%[idx, C, sumD, F] = kmeans(D_mat,k); %edit cluster 'k'
%% Plot Gait Data
x = (1:size(idx,1))/fs * optimal_window;
plot(x,5*idx,'LineWidth',2)

hold on

x2 = (1:size(D,1))/fs; 
plot(x2,D(:,1),'LineWidth',1)

legend('Cluster Assignment','Joint Angle')
title('K-means Clustering of Joint Angle Data')
ylabel('Joint Angle')
xlabel('Time')
%% Plot Seizure Data

x2 = (1:size(D,1))/fs; 

x = (1:size(idx,1))/fs * optimal_window;
yyaxis left
plot(x,idx,'LineWidth',2)

hold on 

yyaxis right
%plot(x2,D(:,1),'LineWidth',1)
plot(t_lfp,lfp)


legend('Cluster Assignment','Seizure Biomarker Dynamics')
title('K-means Clustering of Seizure Biomarker Dynamics Data')
ylabel('Power')
xlabel('Time')

%f(Xk) - f(Xk-1)