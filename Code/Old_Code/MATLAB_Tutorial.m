function stats = MATLAB_Tutorial(data)
% MATLAB_Tutorial Compute descriptive statistics on input data
% stats = MATLAB_Tutorial(data) displays and returns descriptive
% statistics:
% mean, median, minimum, maximum, count
% INPUTS:
% data: a 1D vector of numbers in single or double precision
% 
% OUTPUTS:
% stats: a vector containing mean, median, minimum, maximum, count
%
% code written by Hithardhi Duggireddy
%
%% checking the input data
% check that data are numeric
if ~isa(data,'numeric')
    help MATLAB_Tutorial
    error('Input must contain numbers!')
end

% check that the input is a vector
if sum(size(data) > 1) > 1
    help MATLAB_Tutorial
    error('Data must be a vector!')
end

%check if the data contains more than a single number
if numel(data) < 2
    help MATLAB_Tutorial
    error('Data must contain more than 1 element!')
end
%% compute the descriptive statisitics
meanval   = mean(data);
medianval = median(data);
minval    = min(data);
maxval    = max(data);
N         = length(data);

%% group outputs into one vector
stats = [meanval medianval minval maxval N];

%% display output stats in order of output
disp(['Mean value is' num2str(meanval) '.'])
disp(['Median value is' num2str(medianval) '.'])
fprintf(['Minimum value is' num2str(minval) '.'])
fprintf('Maximum value is %g.\n', maxval);
fprintf(['Number of numbers:' num2str(N) '.\n'])
disp('Done.')
%% For-Loop
for i = 1:10
    if mod(i,2) == 0, continue
    end
       disp(['Running iteration' num2str(i) '.']) 
end
%% While-Loop
i = 1;
while i < 50
    i = i+1;
    disp(['Give me' num2str(i) ...
    'pieces of chocolate!'])
end




