%% Load Data
experiment_name = 'waldo_2017_11_29_007';
openNSx(['Data/11292017_New/' experiment_name '.ns2'])

%% FFT File Loop
close all

for c1 = 1:size(NS2.Data(:,1),1)/2
    
    fs = NS2.MetaTags.SamplingFreq;
    data = double(NS2.Data(c1,:));
    
    %Filter Data (highpass + notch)
    highpass_filt = designfilt('highpassiir','FilterOrder',4, ...
        'HalfPowerFrequency',5, ...
        'DesignMethod','butter','SampleRate',1000);
    
    notch_filt = designfilt('bandstopiir','FilterOrder',4, ...
        'HalfPowerFrequency1',55, 'HalfPowerFrequency2',65, ...
        'DesignMethod','butter','SampleRate',1000);
    
    hp_data = filtfilt(highpass_filt,data');
    data = filtfilt(notch_filt,hp_data);
    
    t_all = (1:length(data))/fs;
    
    %FFT plot
    N = length(data);
    FFT = fft(data);
    FFT = FFT(1:N/2+1);
    
    %PSD
    power_spec_dx = (1/(fs*N)) * abs(FFT).^2;
    power_spec_dx(2:end-1) = 2*power_spec_dx(2:end-1);
    
    %frequency in hertz
    freq = 0:fs/length(data):fs/2;
    
    subplot(8,4,c1)
    
    %log scale plot of PSD
    plot(freq,10*log10(power_spec_dx))
    xlim([0 50])
    
    grid on
    
    title('Periodogram Using FFT')
    xlabel('Frequency (Hz)')
    ylabel('Power/Frequency (dB/Hz)')
    
end
%% LFP File Loop
close all

for c1 = 1:size(NS2.Data(:,1),1)/2
    
    fs = NS2.MetaTags.SamplingFreq;
    data = double(NS2.Data(c1,:));
    
    %Filter Data (highpass + notch)
    highpass_filt = designfilt('highpassiir','FilterOrder',4, ...
        'HalfPowerFrequency',5, ...
        'DesignMethod','butter','SampleRate',1000);
    
    notch_filt = designfilt('bandstopiir','FilterOrder',4, ...
        'HalfPowerFrequency1',55, 'HalfPowerFrequency2',65, ...
        'DesignMethod','butter','SampleRate',1000);
    
    hp_data = filtfilt(highpass_filt,data');
    data = filtfilt(notch_filt,hp_data);
    
    t_all = (1:length(data))/fs;
    
    %LFP subplot
    subplot(8,4,c1)
    
    %plot entire recording
    plot(t_all,data)
    ylim([-1000 1000])
    xlim([450 800])
    
    title(sprintf('Waldo 11/29/2017 LFP Channel %d',c1));
    xlabel('Time (seconds)');
    ylabel('Amplitude (�V)');
    
end