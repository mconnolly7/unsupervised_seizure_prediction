%% Open NSx file
experiment_name = 'waldo_2017_11_29_007';
openNSx(['Data/11292017_New/' experiment_name '.ns2'])
%% Convert Data to Double
fs = NS2.MetaTags.SamplingFreq;
data = double(NS2.Data(1,:));
X = mean(data); 
data = data - X;
%% Highpass + Notch Filter
highpass_filt = designfilt('highpassiir','FilterOrder',4, ...
      'HalfPowerFrequency', 2, ...
      'DesignMethod','butter','SampleRate',1000);

% lowpass_filt = designfilt('lowpassiir','FilterOrder',4, ...
%        'HalfPowerFrequency',150, ...
%        'DesignMethod','butter','SampleRate',1000);

notch_filt = designfilt('bandstopiir','FilterOrder',4, ...
       'HalfPowerFrequency1',58, 'HalfPowerFrequency2',62, ...
       'DesignMethod','butter','SampleRate',1000);

hp_data = filtfilt(highpass_filt,data');
% lp_data = filtfilt(lowpass_filt,hp_data);
data    = filtfilt(notch_filt,hp_data);

%time x-axis (seconds)
t_all = (1:length(data))/fs;