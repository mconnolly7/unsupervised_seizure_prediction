%% Extract Spectra
w = 5;

X_ii = [ ];

for idx = 1:10
% Cycle Through Struct
[data, sz_start, fs, w] = Seizure_Struct_Extract(seizure, idx, w);

% Window Data
[D] = Interictal_Window(sz_start, data, fs, w);

% Extract Spectral Features
[X_delta, X_theta, X_alpha, X_beta, X_gamma, S] = Data_Band_Extract_Function(D);


X_single = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);

X_ii       = vertcat(X_ii, X_single);
end

X = vertcat(X_pi,X_ii);
Y = vertcat(Y_pi,Y_ii);

%spectral_band.window30_X = X;
%spectral_band_no_sample.window1 = X;