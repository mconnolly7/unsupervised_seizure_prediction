%% Load data

load('/Users/Hith/Box_Sync/Honors_Thesis/Data/NHP_Data/KP/KP_11_25_002/KP_2019_11_25_002_ns3(2000).mat')
data = KP_2019_11_25_002_ns3_2000__Ch8.values;
fs = 2000;
t_all = (1:length(data))/fs;

%% Create Logical Vector for Seizure/No Seizure
sz_annotations           = xlsread('KP_Data_Organization');
sz_annotations           = sz_annotations(:,3:4);

%Specify seizure entry number for recording
sz_annotations_day       = sz_annotations(59:65,:);

%Choose what column from spreadsheet is start and end
sz_start                 = sz_annotations_day(:,1);
sz_end                   = sz_annotations_day(:,2);

%Initialize zeros matrix and time matrix
recording_matrix         = zeros(1,size(t_all,2));
t_rec                    = (1:size(recording_matrix,2))/fs;

%Loop through seizure start and stop
for seizure_idx = 1:size(sz_annotations_day,1)
    recording_matrix(seizure_idx,:) = (t_rec >= sz_start(seizure_idx) & t_rec <= sz_end(seizure_idx));
end

close all

%Sum all rows to get one logical vector with seizure (1) and no seizure (0)
all_seizure = sum(recording_matrix);

%% Ictal Start and End

%Use diff function to specify seizure start (1) and  seizure end (-1)
dx_seizure = diff(all_seizure);

%Return start and end indexes
sz_start   = find(dx_seizure == 1) + 1;
sz_end     = find(dx_seizure <  0) +1;

%Create seizure start and end matrix (one in time and one in samples)
ictal      = [sz_start;sz_end]/fs;
ictal_segment = [sz_start;sz_end];
%y_axis     = ones(2,17)*1000;

%% Interictal + Ictal Segment
sz_idx = 7
n = 65

%first segement
%segment   = data(1:sz_end(1));
%t_segment = (1:size(segment,1));
 
%1+n segments
segment   = data(sz_end(sz_idx-1)+1:sz_end(sz_idx));   %(n-1) and (n)
t_segment = (1:size(segment,1));

%Seizure Struct for Field Corresponding to Interical+Ictal Segment
seizure_new(n).lfp = segment;
seizure_new(n).lfp_time = t_segment;
seizure_new(n).fs = fs;
seizure_new(n).start = ictal(2*sz_idx-1);  %(2*n-1)
seizure_new(n).end = ictal(2*sz_idx); %2*n
seizure_new(n).date = '11_25_002';
seizure_new(n).animal = 'KP';
%seizure_new(sz_idx).event_number = sz_idx;

%figure
%plot(t_segment/fs,segment)

% title('Waldo 11/29/2017 - Recording 7 - LFP Channel 1 - Segment 1')
% ylabel('Amplitude (�V)')
% xlabel('Time (s)')

%% Ictal Segment
close all

%index interictal + ictal segment start and end using seizure start and end matrix 
ictal = data(ictal_segment(35):ictal_segment(36));
t_ictal = (1:size(ictal,1));

%Add ictal fields to seizure struct
seizure_new(sz_idx).ictal = ictal;
seizure_new(sz_idx).ictal_time = t_ictal;

%% Interictal Segment

%first Ii: index interictal + ictal segment start and end using seizure start and end matrix
% interictal = data(1:ictal_segment(1)-1);
% t_interictal = (1:size(interictal,1));

%1+n Ii
interictal = data(ictal_segment(34)+1:ictal_segment(35)-1);
t_interictal = (1:size(interictal,1));

%Add interictal fields to seizure struct
seizure_new(sz_idx).interictal = interictal;
seizure_new(sz_idx).interictal_time = t_interictal;