sz_annotations           = xlsread('waldo_2017_11_29_seizure_table');
sz_annotations_rec       = sz_annotations(1:18,:);
sz_start                 = sz_annotations_rec(:,1);
sz_end                   = sz_annotations_rec(:,2);

recording_matrix         = zeros(1,size(t_all,2));
t_rec                    = (1:size(recording_matrix,2))/fs;


for seizure_idx = 1:size(sz_annotations_rec,1)
    recording_matrix(seizure_idx,:) = (t_rec >= sz_start(seizure_idx) & t_rec <= sz_end(seizure_idx));
end

close all
all_seizure = sum(recording_matrix);

plot(t_all,data)
ylim([-0.25*10^4 0.25*10^4])

hold on

plot(t_rec,all_seizure*1000,'r','Linewidth',3)

%title('Waldo 11/29/2017 - Recording 7 - LFP Channel 1')
%ylabel('Amplitude (�V)')
%xlabel('Time (s)')

%print(gcf,'/Users/Hith/Documents/Beckman_Summer_2019/Figures/Waldo_2017_11_29_009_LFP_Label_Channel_1','-dpdf')