%% Create Logical Vector for Seizure/No Seizure
sz_annotations           = xlsread('waldo_2017_11_29_seizure_table');

%Specify seizure entry number for recording
sz_annotations_day       = sz_annotations(1:18,:);

%Choose what column from spreadsheet is start and end
sz_start                 = sz_annotations_day(:,1);
sz_end                   = sz_annotations_day(:,2);

%Initialize zeros matrix and time matrix
recording_matrix         = zeros(1,size(t_all,2));
t_rec                    = (1:size(recording_matrix,2))/fs;

%Loop through seizure start and stop 
for seizure_idx = 1:size(sz_annotations_day,1)
    recording_matrix(seizure_idx,:) = (t_rec >= sz_start(seizure_idx) & t_rec <= sz_end(seizure_idx));
end

close all

%Sum all rows to get one logical vector with seizure (1) and no seizure (0)
all_seizure = sum(recording_matrix);

%% Ictal Annotations
dx_seizure = diff(all_seizure);

%Return start and end indexes
sz_start   = find(dx_seizure == 1) + 1;
sz_end     = find(dx_seizure <  0) +1;

%Create seizure start and end matrix
ictal      = [sz_start;sz_end]/fs;
y_axis     = ones(2,18)*1000;

%% Interictal Annotation
dx_seizure = diff(all_seizure);
Ii_start    = find(dx_seizure == 1) + 1;
Ii_end      = find(dx_seizure <  0) + 1;

%Remove first seizure start entry
Ii_start(1) = [];

%Remove last seizure end entry
Ii_end(end) = [];

%Difference between remaining seizure start and end times will give vector of interictal durations
interictal  = (Ii_start - Ii_end)/fs;

%% Plot Individual Lines Above Ictal Period
plot(t_all,data)
ylim([-0.25*10^4 0.25*10^4])

hold on

plot(ictal,y_axis,'r','LineWidth',5);

title('Waldo 11/29/2017 - Recording 7 - LFP Channel 1')
ylabel('Amplitude (�V)')
xlabel('Time (s)')

print(gcf,'/Users/Hith/Documents/Beckman_Summer_2019/Figures/Waldo_2017_11_29_007_LFP_Label_Channel_1','-dpdf')