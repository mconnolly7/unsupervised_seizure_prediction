function [X_roc, Y_roc, AUC_roc] = Logistic_Regression_Val(X,Y)

cv_idx = crossvalind('Kfold',size(X,1),5);

for c1 = 1:max(cv_idx)
    
    train_idx = cv_idx ~= c1;
    test_idx  = cv_idx == c1;
    
    % X is my vector of extracted features; Y is my vector of labels
    X_train = X(train_idx,:);
    Y_train = Y(train_idx,:);
    
    X_test = X(test_idx,:);
    Y_test = Y(test_idx,:);
    
    b     = glmfit(X_train,Y_train,'binomial','link','logit');
    Y_hat = glmval(b,X_test,'logit');
    
    % Label
    y_all_test{c1} = Y_test; 
    
    % Ground Truth Estimate
    y_all_hat{c1}  = Y_hat;
    
end

[X_roc,Y_roc,~,AUC_roc] = perfcurve(y_all_test,y_all_hat,1);
end

%figure
%plot(X_roc,Y_roc)

    
%print('/Users/Hith/Documents/SURE 2018/Poster/Figures/Figure 1: Color_Plot_Gait_Final','-dpng','-r500')

