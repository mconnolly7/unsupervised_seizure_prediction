%% Load Data
function [data, sz_start, fs, w] = Seizure_Struct_Extract(seizure, idx, w)

data = seizure(idx).lfp;

sz_start = seizure(idx).start;

% if idx == 1
% sz_start = seizure(idx).start;
% else
%     sz_start = seizure(idx).start - seizure(idx-1).end;
% end

fs = 2000;

end

%% Load Cell Data
% function [data, sz_start, fs, w] = Seizure_Cell_Extract(data_all, idx, w)
% 
% data = data_all(idx).lfp;
% 
% if idx == 1
% sz_start = seizure(idx).start;
% else
%     sz_start = seizure(idx).start - seizure(idx-1).end;
% end
% 
% fs = 2000;
% 
% end