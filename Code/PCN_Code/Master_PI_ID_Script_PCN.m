%% Load Data
load('/Users/Hith/Box_Sync/Honors_Thesis/Data/NHP_Data/KP_seizure_struct_adjusted_start.mat')

%% Extract Pre-Ictal Spectra
w = 5;

X_pi = [ ];

for idx = 1:25
% Cycle Through Struct
[data, sz_start, fs, w] = Seizure_Struct_Extract(seizure, idx, w);

% Window Data
[D] = Pre_Seizure_Window(sz_start, data, fs, w);

% Extract Spectral Features
[X_delta, X_theta, X_alpha, X_beta, X_gamma, S] = Data_Band_Extract_Function(D);


X_single = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);

X_pi       = vertcat(X_pi, X_single);

end

Y_pi = ones(size(X_pi,1),1);
Y_ii = zeros(size(X_pi,1),1);

%% Extract Interictal Spectra
w = 5;

X_ii = [ ];

for idx = 1:25
% Cycle Through Struct
[data, sz_start, fs, w] = Seizure_Struct_Extract(seizure, idx, w);

% Window Data
[D] = Interictal_Window(sz_start, data, fs, w);

% Extract Spectral Features
[X_delta, X_theta, X_alpha, X_beta, X_gamma, S] = Data_Band_Extract_Function(D);


X_single = horzcat(X_delta, X_theta, X_alpha, X_beta, X_gamma);

X_ii       = vertcat(X_ii, X_single);
end

X = vertcat(X_pi,X_ii);
Y = vertcat(Y_pi,Y_ii);