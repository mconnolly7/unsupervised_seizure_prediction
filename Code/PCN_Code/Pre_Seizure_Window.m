%% Pre-Seizure Window
function [D] = Pre_Seizure_Window(sz_start, data, fs, w)

for idx = 1
sz_start = ceil(sz_start);
%pre_sz_start = sz_start - w*fs*(idx);
pre_sz_start = sz_start - w*(idx);


%pre_sz_window = data(pre_sz_start:sz_start);
pre_sz_window = data(fs*pre_sz_start:fs*sz_start);

%t_pre_sz = (1:length(pre_sz_window));

%patch_pre_sz = patch([pre_sz_start,sz_start,sz_start,pre_sz_start],[-3,-3,3,3],'m','FaceAlpha',0.5);
%hold on
end

D = pre_sz_window;

end
